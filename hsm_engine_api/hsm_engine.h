#ifndef HSM_ENGINE_H
#define HSM_ENGINE_H

/**
 *  @file hsm_engine.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential@n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup hsm_engine_api hsm_engine Interface Documentation
 *  @{
 *      @details Interface to the Heirarchical State Machine Engine
 *
 *      @page ABBR Abbreviations
 *        - HSM Heirarchical State Machine
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine_cfg.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/
/**
 * Value used for a transition that has no trigger
 */
#define HSM_NO_TRIGGER ((HSM_Trigger_T)-1)

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    HSM_INITIAL,   /**< Initial Pseudostate */
    HSM_SIMPLE,    /**< Simple state, has no children */
    HSM_COMPOSITE, /**< Composite state, has children */
    HSM_JUNCTION,  /**< Junction Pseudostate */
    HSM_ENTRY,     /**< Entry Pseudostate */
    HSM_EXIT       /**< Exit Pseudostate */
} HSM_State_Kind_T;

typedef enum
{
    HSM_SUCCESS,               /**< Process was completed successfully */
    HSM_VALID_CHAIN_NOT_FOUND, /**< No valid transition chain was found */
    HSM_CHAIN_TOO_LONG,        /**< Process resulted in a transition chain with a length that is not supported */
    HSM_TRIGGER_UNHANDLED,     /**< Trigger Event was not handled */
    HSM_NUM_STATUS
} HSM_Status_T;

/**
 * integer ID of a trigger event for an HSM
 */
typedef int32_t HSM_Trigger_T;

/**
 * Pointer to a Guard Constraint function
 *
 * @param [in] Trigger - Event ID being processed by the HSM
 * @param [in] Trigger_Data - Data associated with the Trigger
 *
 * @return value indicating if the transition can be enabled
 * @retval true - Transition can be enabled
 * @retval false - Transition cannot be enabled
 */
typedef bool_t (*HSM_Guard_Constraint_T)(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/**
 * Function pointer for a Transition, state Entry and state Exit Behavior
 */
typedef void (*HSM_Behavior_T)(void);

typedef struct
{
    HSM_Trigger_T const          Trigger; /**< Event that triggers this transition */
    HSM_Guard_Constraint_T const Guard;   /**< pointer to a guard constraint for this transition */
    HSM_Behavior_T const         Effect;  /**< pointer to a behavior function for the transition effect */
    struct HSM_State_Tag const * Source;  /**< pointer to the state from which the transition originates */
    struct HSM_State_Tag const * Target;  /**< pointer to the state at which the transition terminates */
} HSM_Transition_T;

typedef struct HSM_State_Tag
{
    HSM_State_Kind_T const       Kind;                     /**< The kind of state this is */
    struct HSM_State_Tag const * Initial_State;            /**< pointer to any initial state this state has. HSM_COMPOSITE state only */
    struct HSM_State_Tag const * Parent_State;             /**< pointer to the parent state of this state */
    uint32_t const               Num_Outbound_Transitions; /**< number of transitions that exit this state*/
    HSM_Transition_T const *     Outbound_Transitions;     /**< pointer to the array of transitions that exit this state */
    uint32_t const               Num_Internal_Transitions; /**< number of transitions that do not exit this state */
    HSM_Transition_T const *     Internal_Transitions;     /**< pointer to the array of transitions that do not exit this state */
    HSM_Behavior_T const         Entry_Behavior;           /**< pointer to the Entry Behavior function of this state */
    HSM_Behavior_T const         Exit_Behavior;            /**< pointer to the Exit Behavior function of this state */
    char const *                 Name;                     /**< pointer to the C String name of this state */
} HSM_State_T;

typedef struct
{
    HSM_Transition_T const * Transition;                       /**< A transition being evaluated */
    uint32_t                 Target_Outbound_Transition_Index; /**< Target of this Transition that is being evaluated for potential traversal */
} HSM_Transition_Chain_Item_T;

typedef struct
{
    HSM_State_T const *          Current_State;                 /**< Specific simple state that is currently stable */
    struct HSM_State_Tag const * Initial_State;                 /**< Starting Initial Pseudostate of the HSM */
    struct
    {
         HSM_Transition_Chain_Item_T Transition_Chain[HSM_MAX_TRANSITION_CHAIN_LENGTH]; /**< Currently fixed size transition chain to be activated in response to an event */
         size_t                      Chain_Size;                                        /**< Number of transitions in the list */
    } Transition_Chain_Data;
} HSM_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/**
 * Performs the initial transition of the provided Heirarchical Statemachine
 *
 * @param[in] HSM - Heirarchical Statemachine to start
 *
 * @return - indicates if the HSM was successfully started or not
 * @retval true - HSM started successfully and traversed the initial transition to a stable state
 * @retval false - HSM was not able to traverse an initial transition because no stable state was found
 */
bool_t HSM_Start(HSM_T * HSM);

/**
 * Provide the Event and Event_Data to the HSM to be processed, potentially triggering a transition
 * to another stable state.
 *
 * @param [in] HSM - Heirarchical Statemachine that will process the provided Event and Event_Data
 * @param [in] Event - Event to be processed
 * @param [in] Event_Data - Data for the Event being processed
 */
void HSM_Process_Event(HSM_T * HSM, HSM_Trigger_T Event, uint32_t Event_Data);

/** @} doxygen end group */

#endif /* HSM_ENGINE_H */
