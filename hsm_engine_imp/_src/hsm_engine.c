/**
 *  @file hsm_engine.c
 *
 *  @ref hsm_engine_api
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup hsm_engine_imp hsm_engine Implementation
 *  @{
 *
 *      System specific implementation of the @ref hsm_engine_api
 *
 *      @page TRACE Traceability
 *        - Design Document(s):
 *          - None
 *
 *        - Applicable Standards (in order of precedence: highest first):
 *          - @http{sharepoint/sandbox/ghspdev/LaunchPad/EngineeringDepartment/Elec_proj_mgmt/Revision%20Controlled%20Library/Software%20Process/Work%20Instructions/Coding%20Standard.docx,
 *                  "GHSP Coding Standard"}
 *
 *      @page DFS Deviations from Standards
 *        - None
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"
#include <string.h>

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/
STATIC HSM_Status_T hsm_Build_Transition(HSM_T * HSM,
                                         HSM_Transition_T const * Start_Transition,
                                         HSM_Trigger_T Event,
                                         uint32_t Event_Data);

STATIC bool_t hsm_Transition_Is_Enabled(HSM_Transition_T const * Transition, HSM_Trigger_T Event, uint32_t Event_Data);

STATIC bool_t hsm_Add_Transition_To_Chain(HSM_T * HSM, HSM_Transition_T const * Transition);

STATIC void hsm_Remove_Last_Transition_From_Chain(HSM_T * HSM);

STATIC void hsm_Clear_Transition_Chain(HSM_T * HSM);

STATIC void hsm_Execute_Transition_Chain(HSM_T * HSM);

STATIC HSM_State_T const * hsm_Find_Least_Common_Ancestor(HSM_State_T const * State_1, HSM_State_T const * State_2);

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
/*
 * Please see hsm_engine.h for more information about this function.
 */
bool_t HSM_Start(HSM_T * HSM)
{
    bool_t hsm_started = false;

    if (    (NULL != HSM)
         && (NULL != HSM->Initial_State))
    {
        bool_t Simple_State_Found = false;
        bool_t Initial_Transition_Failed = false;
        HSM_Transition_T const * Initial_Transition = &HSM->Initial_State->Outbound_Transitions[0];

        if (HSM_SUCCESS == hsm_Build_Transition(HSM, Initial_Transition, HSM_NO_TRIGGER, 0))
        {
            HSM_State_T const * Target_State = HSM->Transition_Chain_Data.Transition_Chain[HSM->Transition_Chain_Data.Chain_Size - 1].Transition->Target;

            hsm_Execute_Transition_Chain(HSM);

            HSM->Current_State = Target_State;

            hsm_started = true;
        }
    }

    return hsm_started;
}

/*
 * Please see hsm_engine.h for more information about this function.
 */
void HSM_Process_Event(HSM_T * HSM, HSM_Trigger_T Event, uint32_t Event_Data)
{
    HSM_State_T const * Scanning_State = HSM->Current_State;
    HSM_Status_T External_Transition_Status = HSM_TRIGGER_UNHANDLED;
    HSM_Status_T Internal_Transition_Status = HSM_TRIGGER_UNHANDLED;

    while (    (Scanning_State != NULL)/* Reached root */
            && (HSM_SUCCESS != Internal_Transition_Status)) /* An internal transition handled the Trigger */
    {
        uint_fast8_t Transition_Index = 0;
        HSM_Status_T Transition_Status = HSM_VALID_CHAIN_NOT_FOUND;

        /* Clear the transition list */
        hsm_Clear_Transition_Chain(HSM);

        /* Scan internal transitions for one that handles the trigger */
        while (    (HSM_SUCCESS != Internal_Transition_Status)
                && (Transition_Index < Scanning_State->Num_Internal_Transitions))
        {
            if (    (Event == Scanning_State->Internal_Transitions[Transition_Index].Trigger) /* Trigger matches this transition's trigger */
                 && (    (NULL == Scanning_State->Internal_Transitions[Transition_Index].Guard) /* No guard always evaluates to true */
                      || (Scanning_State->Internal_Transitions[Transition_Index].Guard(Event, Event_Data))))
            {
                /* Execute the Internal Transition effect */
                Scanning_State->Internal_Transitions[Transition_Index].Effect();
                Internal_Transition_Status = HSM_SUCCESS;
            }

            Transition_Index++;
        }

        /* Was the event handled by an Internal Transition? */
        if (HSM_SUCCESS != Internal_Transition_Status)
        {
            /* scan outbound transitions for one that handles the trigger */
            Transition_Index = 0;

            while (    (HSM_SUCCESS != External_Transition_Status)
                    && (Transition_Index < Scanning_State->Num_Outbound_Transitions))
            {
                if (    (Event == Scanning_State->Outbound_Transitions[Transition_Index].Trigger) /* Trigger matches this transition's trigger */
                     && (    (NULL == Scanning_State->Outbound_Transitions[Transition_Index].Guard) /* No guard always evaluates to true */
                          || (Scanning_State->Outbound_Transitions[Transition_Index].Guard(Event, Event_Data))))
                {
                    External_Transition_Status = hsm_Build_Transition(HSM, &Scanning_State->Outbound_Transitions[Transition_Index], Event, Event_Data);
                }

                Transition_Index++;
            }

            if (HSM_SUCCESS == External_Transition_Status)
            {
                HSM_State_T const * Target_State = HSM->Transition_Chain_Data.Transition_Chain[HSM->Transition_Chain_Data.Chain_Size - 1].Transition->Target;
                HSM_State_T const * LCA = hsm_Find_Least_Common_Ancestor(HSM->Current_State, Target_State);

                while (HSM->Current_State != LCA)
                {
                    if (NULL != HSM->Current_State->Exit_Behavior)
                    {
                        HSM->Current_State->Exit_Behavior();
                    }

                    HSM->Current_State = HSM->Current_State->Parent_State;
                }

                hsm_Execute_Transition_Chain(HSM);

                HSM->Current_State = Target_State;

                break;
            }

        }

        /* Was the Trigger handled at this level of the heirarchy? */
        if (    (HSM_SUCCESS != Internal_Transition_Status)
             && (HSM_SUCCESS != External_Transition_Status))
        {
            /* Trigger not handled at this level of the heirarchy. Try the parent state */
            Scanning_State = Scanning_State->Parent_State;
        }
    }
}

/**
 * Follows the transition to the stable state configuration Entering all states along the way and performing
 * all transition Activities as necessary.
 *
 * @param [in/out] HSM - Statemachine whose transition chain is to be executed.
 */
STATIC void hsm_Execute_Transition_Chain(HSM_T * HSM)
{
    uint_fast8_t Transition_Index = 0;
    HSM_Transition_Chain_Item_T * Transition_Chain = HSM->Transition_Chain_Data.Transition_Chain;

    for (Transition_Index = 0; Transition_Index < HSM->Transition_Chain_Data.Chain_Size; Transition_Index++)
    {
        /* If transition has an effect, execute it */
        if(NULL != Transition_Chain[Transition_Index].Transition->Effect)
        {
            Transition_Chain[Transition_Index].Transition->Effect();
        }

        /* If the target of the transition has an entry, execute it */
        if (NULL != Transition_Chain[Transition_Index].Transition->Target->Entry_Behavior)
        {
            Transition_Chain[Transition_Index].Transition->Target->Entry_Behavior();
        }
    }

    /* Mark Transition List as empty */
    hsm_Clear_Transition_Chain(HSM);
}

/**
 * Finds the least common ancestor of the two supplied states
 * The least common ancestor is the state that contains both of the two states.
 *
 * @pre State_1 != NULL
 * @pre State_2 != NULL
 *
 * @param [in] State_1 - First state to find the LCA for
 * @param [in] State_2 - Second state to find the LCA for
 *
 * @return pointer to the state that is the LCA of the two provided states. NULL is the root node, i.e. the statemachine itself.
 */
STATIC HSM_State_T const * hsm_Find_Least_Common_Ancestor(HSM_State_T const * State_1, HSM_State_T const * State_2)
{
    HSM_State_T const * LCA = NULL;
    HSM_State_T const * LCA_Candidate = State_1->Parent_State;

    while (    (LCA_Candidate != NULL)
            && (NULL == LCA))
    {
        HSM_State_T const * Search_State = State_2;

        while (Search_State != NULL)
        {
            if (LCA_Candidate != Search_State)
            {
                Search_State = Search_State->Parent_State;
            }
            else
            {
                LCA = LCA_Candidate;
                break;
            }
        }
        LCA_Candidate = LCA_Candidate->Parent_State;
    }

    return LCA;
}

/**
 * Adds a single transition to the transition chain of the supplied HSM.
 *
 * @pre HSM != NULL
 * @pre Transition != NULL
 *
 * @param [in/out] HSM - Add the Transition to this HSM's transition chain
 * @param [in] Transition - Transition to add to the transition chain
 *
 * @return Transition was placed in the transition chain (true) or not (false)
 */
STATIC bool_t hsm_Add_Transition_To_Chain(HSM_T * HSM, HSM_Transition_T const * Transition)
{
    bool_t Success = false;
    size_t Insert_Index = HSM->Transition_Chain_Data.Chain_Size;

    if (Insert_Index < HSM_MAX_TRANSITION_CHAIN_LENGTH)
    {
        HSM->Transition_Chain_Data.Transition_Chain[Insert_Index].Transition = Transition;
        HSM->Transition_Chain_Data.Transition_Chain[Insert_Index].Target_Outbound_Transition_Index = 0;
        HSM->Transition_Chain_Data.Chain_Size++;

        Success = true;
    }

    return Success;
}

/**
 * Erases the last entry of the transition chain from the HSM
 *
 * @pre HSM != NULL
 *
 * @param [in/out] HSM - Remove the last transition from this HSM's transition chain
 */
STATIC void hsm_Remove_Last_Transition_From_Chain(HSM_T * HSM)
{
    HSM->Transition_Chain_Data.Chain_Size--;
    HSM->Transition_Chain_Data.Transition_Chain[HSM->Transition_Chain_Data.Chain_Size].Transition = NULL;
    HSM->Transition_Chain_Data.Transition_Chain[HSM->Transition_Chain_Data.Chain_Size].Target_Outbound_Transition_Index = 0;
}

/**
 * Erases all elements of the HSM's transition chain
 *
 * @pre HSM != NULL
 *
 * @param [in/out] HSM - erase the transition chain of this HSM
 */
STATIC void hsm_Clear_Transition_Chain(HSM_T * HSM)
{
    uint_fast8_t Transition_Index = 0;

    for (Transition_Index = 0; Transition_Index < HSM_MAX_TRANSITION_CHAIN_LENGTH; Transition_Index++)
    {
        HSM->Transition_Chain_Data.Transition_Chain[Transition_Index].Target_Outbound_Transition_Index = 0;
        HSM->Transition_Chain_Data.Transition_Chain[Transition_Index].Transition = NULL;
    }

    HSM->Transition_Chain_Data.Chain_Size = 0;
}

/**
 * Determines if a provided transition is enabled. An enabled transition is one that has one of the two following properties
 *
 * - Transition has no trigger
 * - Transition has a trigger that matches the current Event
 *
 * and one of the two following properties
 * - The transition has no Guard
 * - The transition has a Guard that is true.
 *
 * @pre Transition != NULL
 *
 * @param [in] Transition - The transition to be evaluated
 * @param [in] Event - the event being processed
 * @param [in] Event_Data - integer data associated with the Event
 *
 * @return indicates if the transition is enabled (true) or not (false)
 */
STATIC bool_t hsm_Transition_Is_Enabled(HSM_Transition_T const * Transition, HSM_Trigger_T Event, uint32_t Event_Data)
{
    bool_t Is_Enabled = false;

    if (    (    (Event == Transition->Trigger) /* Trigger matches this transition's trigger */
              || (HSM_NO_TRIGGER == Transition->Trigger)) /* Transition has no trigger */
         && (    (NULL == Transition->Guard) /* No guard always evaluates to true */
              || (Transition->Guard(Event, Event_Data))))
    {
        Is_Enabled = true;
    }

    return Is_Enabled;
}

/**
 * Attempts to build a transition chain from one stable state configuration to another stable
 * state configuration, if one can be found. The function will use the provided Start_Transition
 * for evaluation. For a transition chain to be enabled all guards in the chain must be true.
 *
 * @pre HSM != NULL
 * @pre The Start_Transition provided is an enabled outbound transition of the current state.
 *
 * @param [in/out] HSM - The Heirarchical State Machine that is handling the provided Event
 * @param [in] Start_Transition - The first transition to use in the chain
 * @param [in] Event - Event being processed
 * @param [in] Event_Data - integer data associated with the Event
 *
 * @return Value indicating the status of the Transition Chain being built
 * @retval HSM_SUCCESS - A chain was built from one stable state configuration to another. The resulting chain may be traversed
 * @retval HSM_CHAIN_TOO_LONG - A valid transition chain may have been found, but there are too many transitions in it.
 * @retval HSM_VALID_CHAIN_NOT_FOUND - Provided Start_Transition did not yield a valid chain to another stable state configuration
 */
STATIC HSM_Status_T hsm_Build_Transition(HSM_T * HSM, HSM_Transition_T const * Start_Transition, HSM_Trigger_T Event, uint32_t Event_Data)
{
    HSM_State_T const * State_In_Process = Start_Transition->Target;
    uint32_t Transition_List_Index = 0;
    bool_t Transition_Chain_Complete = false;
    bool_t Transition_Failed = false;
    HSM_Transition_Chain_Item_T * Transition_Chain = HSM->Transition_Chain_Data.Transition_Chain;
    HSM_Status_T result = HSM_SUCCESS;

    /* Clear the Transition chain */
    hsm_Clear_Transition_Chain(HSM);

    if (!hsm_Add_Transition_To_Chain(HSM, Start_Transition))
    {
        Transition_Failed = true;
        result = HSM_CHAIN_TOO_LONG;
    }

    while (    (!Transition_Chain_Complete)
            && (!Transition_Failed))
    {
        switch (State_In_Process->Kind)
        {
            case HSM_INITIAL:
            {
                if (!hsm_Add_Transition_To_Chain(HSM, &State_In_Process->Outbound_Transitions[0]))
                {
                    Transition_Failed = true;
                    result = HSM_CHAIN_TOO_LONG;
                }
                else
                {
                    Transition_List_Index++;
                    State_In_Process = State_In_Process->Outbound_Transitions[0].Target;
                }
            }
            break;

            case HSM_JUNCTION:
            {
                uint32_t * Outbound_Transition_Index = &Transition_Chain[Transition_List_Index].Target_Outbound_Transition_Index;

                if (hsm_Transition_Is_Enabled(&State_In_Process->Outbound_Transitions[*Outbound_Transition_Index], Event, Event_Data))
                {
                    if (!hsm_Add_Transition_To_Chain(HSM, &State_In_Process->Outbound_Transitions[*Outbound_Transition_Index]))
                    {
                        Transition_Failed = true;
                        result = HSM_CHAIN_TOO_LONG;
                    }
                    else
                    {

                        Transition_List_Index++;

                        /* Move to the target of this transition */
                        State_In_Process = State_In_Process->Outbound_Transitions[*Outbound_Transition_Index].Target;
                    }
                }
                else
                {
                    /* Move to next outbound transition of the target junction */
                    (*Outbound_Transition_Index)++;

                    /* Are there any remaining transitions out of the junction? */
                    if (*Outbound_Transition_Index >= Transition_Chain[Transition_List_Index].Transition->Target->Num_Outbound_Transitions)
                    {
                        /* No remaining outbound transitions to process */

                        /* Move back until there is a junction with unchecked outbound transitions */
                        while (    (HSM->Transition_Chain_Data.Chain_Size != 0)
                                && (    (State_In_Process->Kind == HSM_INITIAL) /* Initial State of Composite State */
                                     || (    (State_In_Process->Kind == HSM_JUNCTION) /* Junction with remaining outbound transitions */
                                          && (*Outbound_Transition_Index >= Transition_Chain[Transition_List_Index].Transition->Target->Num_Outbound_Transitions)))) /* Junction with unchecked outbound transitions */
                        {
                            /* Move back to a previous junction */
                            State_In_Process = Transition_Chain[Transition_List_Index].Transition->Source;
                            Transition_List_Index--;
                            hsm_Remove_Last_Transition_From_Chain(HSM);

                            if (State_In_Process->Kind == HSM_JUNCTION)
                            {
                                Outbound_Transition_Index = &Transition_Chain[Transition_List_Index].Target_Outbound_Transition_Index;
                                (*Outbound_Transition_Index)++;
                            }
                        }

                        if (0 == HSM->Transition_Chain_Data.Chain_Size)
                        {
                            Transition_Failed = true;
                            result = HSM_VALID_CHAIN_NOT_FOUND;
                        }
                    }
                }
            }
            break;

            case HSM_COMPOSITE:
            {
                /* Move to the initial state of the Composite State */
                State_In_Process = State_In_Process->Initial_State;
            }
            break;

            case HSM_SIMPLE:
            {
                /* Transition found. Chain is valid */
                Transition_Chain_Complete = true;
            }
            break;

            default:
            {
                /* Kind not supported in transition */
                Transition_Failed = true;
                result = HSM_VALID_CHAIN_NOT_FOUND;
            }
        }
    }

    if (Transition_Failed)
    {
        hsm_Clear_Transition_Chain(HSM);
    }

    return result;
}

/** @} doxygen end group */
