#ifndef COMPOUND_TRANSITION_HSM_H
#define COMPOUND_TRANSITION_HSM_H
/**
 *  @file compound_transition_hsm.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup compound_transition_hsm_imp
 *  @{
 *
 *      API that exposes the prototypes of all Guard and Behavior functions
 *      of the Compound Transition HSM and the data structures used for mocking
 *      and testing.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    CTH_LEAVE_A_TRIGGER,
    CTH_LEAVE_B_TRIGGER,
    CTH_LEAVE_C_TRIGGER,
    CTH_LEAVE_D_TRIGGER
} Compound_Transition_HSM_Trigger_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
HSM_State_T const CTH_State_A;
HSM_State_T const CTH_State_B;
HSM_State_T const CTH_State_C;
HSM_State_T const CTH_State_D;
HSM_State_T const CTH_State_Initial;
HSM_State_T const CTH_State_J1;
HSM_State_T const CTH_State_J2;
HSM_State_T const CTH_State_J3;
HSM_State_T const CTH_State_J4;
HSM_State_T const CTH_State_J5;
HSM_State_T const CTH_State_J6;

HSM_Transition_T const CTH_State_A_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_B_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_C_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_D_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_Initial_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_J1_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_J2_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_J3_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_J4_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_J6_Outbound_Transitions[1];
HSM_Transition_T const CTH_State_J5_Outbound_Transitions[1];

HSM_T Compound_Transition_HSM;
/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/*-----------------------*\
 * State Entry functions *
\*-----------------------*/
void CTH_A_Entry(void);
void CTH_B_Entry(void);
void CTH_C_Entry(void);
void CTH_D_Entry(void);

/*-----------------------*\
 * State Exit functions *
\*-----------------------*/
void CTH_A_Exit(void);
void CTH_B_Exit(void);
void CTH_C_Exit(void);
void CTH_D_Exit(void);

/*----------------------------*\
 * Transition Guard functions *
\*----------------------------*/
bool_t CTH_A_J2_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CTH_J2_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CTH_J3_J4_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CTH_J5_D_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/*-----------------------------*\
 * Transition Effect functions *
\*-----------------------------*/
void CTH_A_J2_Effect(void);
void CTH_D_J6_Effect(void);
void CTH_Initial_Effect(void);
void CTH_J1_A_Effect(void);
void CTH_J2_B_Effect(void);
void CTH_J4_C_Effect(void);
void CTH_J5_D_Effect(void);
/** @} doxygen end group */
#endif /* COMPOUND_TRANSITION_HSM_H */
