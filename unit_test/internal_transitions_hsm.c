/**
 *  @file internal_transitions_hsm.c
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "internal_transitions_hsm.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*-------------------------------------------*\
 * Begin: Internal Transitions Statemachine definition *
\*-------------------------------------------*/

HSM_Transition_T const ITH_State_A_Internal_Transitions[2] =
{
    {
        .Trigger = INTERNAL_A_TRIGGER,
        .Guard = ITH_Internal_A_Guard,
        .Effect = ITH_Internal_A_Effect,
        .Source = &ITH_State_A,
        .Target = &ITH_State_A,
    },
    {
        .Trigger = MULTILEVEL_TRIGGER,
        .Guard = ITH_Multilevel_A_Guard,
        .Effect = ITH_Multilevel_A_Effect,
        .Source = &ITH_State_A,
        .Target = &ITH_State_A,
    },
};

HSM_Transition_T const ITH_State_A_Outbound_Transitions[1] =
{
    {
        .Trigger = MULTILEVEL_TRIGGER,
        .Guard = ITH_A_B_Guard,
        .Effect = ITH_A_B_Effect,
        .Source = &ITH_State_A,
        .Target = &ITH_State_B,
    },
};

HSM_Transition_T const ITH_State_AA_Internal_Transitions[1] =
{
    {
        .Trigger = MULTILEVEL_TRIGGER,
        .Guard = ITH_AA_Internal_Guard,
        .Effect = ITH_AA_Internal_Effect,
        .Source = &ITH_State_AA,
        .Target = &ITH_State_AA,
    },
};

HSM_Transition_T const ITH_State_AA_Outbound_Transitions[1] =
{
    {
        .Trigger = MULTILEVEL_TRIGGER,
        .Guard = ITH_AA_AB_Guard,
        .Effect = ITH_AA_AB_Effect,
        .Source = &ITH_State_AA,
        .Target = &ITH_State_AB,
    },
};

HSM_Transition_T const ITH_State_Initial_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &ITH_State_Initial,
        .Target = &ITH_State_A,
    },
};

HSM_Transition_T const ITH_State_InitialA_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &ITH_State_InitialA,
        .Target = &ITH_State_AA,
    },
};

HSM_State_T const ITH_State_A =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &ITH_State_InitialA,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(ITH_State_A_Outbound_Transitions),
    .Outbound_Transitions = ITH_State_A_Outbound_Transitions,
    .Num_Internal_Transitions = Num_Elems(ITH_State_A_Internal_Transitions),
    .Internal_Transitions = ITH_State_A_Internal_Transitions,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "A",
};

HSM_State_T const ITH_State_AA =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &ITH_State_A,
    .Num_Outbound_Transitions = Num_Elems(ITH_State_AA_Outbound_Transitions),
    .Outbound_Transitions = ITH_State_AA_Outbound_Transitions,
    .Num_Internal_Transitions = Num_Elems(ITH_State_AA_Internal_Transitions),
    .Internal_Transitions = ITH_State_AA_Internal_Transitions,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "AA",
};

HSM_State_T const ITH_State_AB =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &ITH_State_A,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "AB",
};

HSM_State_T const ITH_State_B =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "B",
};

HSM_State_T const ITH_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = ITH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const BTH_State_InitialA =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = ITH_State_InitialA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "InitialA",
};

HSM_T Internal_Transtions_HSM =
{
    .Current_State = NULL,
    .Initial_State = &ITH_State_Initial,
};

/*-----------------------------------------*\
 * End: Internal_Transitions Statemachine definition *
\*-----------------------------------------*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
