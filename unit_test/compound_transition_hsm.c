/**
 *  @file composite_state_hsm.c
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "compound_transition_hsm.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*----------------------------------------------------*\
 * Begin: Compound Transition Statemachine definition *
\*----------------------------------------------------*/
HSM_Transition_T const CTH_State_A_Outbound_Transitions[1] =
{
    {
        .Trigger = CTH_LEAVE_A_TRIGGER,
        .Guard = CTH_A_J2_Guard,
        .Effect = CTH_A_J2_Effect,
        .Source = &CTH_State_A,
        .Target = &CTH_State_J2,
    },
};

HSM_Transition_T const CTH_State_B_Outbound_Transitions[1] =
{
    {
        .Trigger = CTH_LEAVE_B_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &CTH_State_B,
        .Target = &CTH_State_J3,
    },
};

HSM_Transition_T const CTH_State_C_Outbound_Transitions[1] =
{
    {
        .Trigger = CTH_LEAVE_C_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &CTH_State_C,
        .Target = &CTH_State_J5,
    },
};

HSM_Transition_T const CTH_State_D_Outbound_Transitions[1] =
{
    {
        .Trigger = CTH_LEAVE_D_TRIGGER,
        .Guard = NULL,
        .Effect = CTH_D_J6_Effect,
        .Source = &CTH_State_D,
        .Target = &CTH_State_J6,
    },
};

HSM_Transition_T const CTH_State_Initial_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CTH_Initial_Effect,
        .Source = &CTH_State_Initial,
        .Target = &CTH_State_J1,
    },
};

HSM_Transition_T const CTH_State_J1_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CTH_J1_A_Effect,
        .Source = &CTH_State_J1,
        .Target = &CTH_State_A,
    },
};

HSM_Transition_T const CTH_State_J2_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = CTH_J2_B_Guard,
        .Effect = CTH_J2_B_Effect,
        .Source = &CTH_State_J2,
        .Target = &CTH_State_B,
    },
};

HSM_Transition_T const CTH_State_J3_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = CTH_J3_J4_Guard,
        .Effect = NULL,
        .Source = &CTH_State_J3,
        .Target = &CTH_State_J4,
    },
};

HSM_Transition_T const CTH_State_J4_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CTH_J4_C_Effect,
        .Source = &CTH_State_J4,
        .Target = &CTH_State_C,
    },
};

HSM_Transition_T const CTH_State_J5_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = CTH_J5_D_Guard,
        .Effect = CTH_J5_D_Effect,
        .Source = &CTH_State_J5,
        .Target = &CTH_State_D,
    },
};

HSM_Transition_T const CTH_State_J6_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &CTH_State_J6,
        .Target = &CTH_State_A,
    },
};

HSM_State_T const CTH_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CTH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const CTH_State_J1 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_J1_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_J1_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const CTH_State_A =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_A_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_A_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CTH_A_Entry,
    .Exit_Behavior = CTH_A_Exit,
    .Name = "A",
};

HSM_State_T const CTH_State_J2 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_J2_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_J2_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J2",
};

HSM_State_T const CTH_State_B =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_B_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_B_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CTH_B_Entry,
    .Exit_Behavior = CTH_B_Exit,
    .Name = "B",
};

HSM_State_T const CTH_State_J3 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_J3_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_J3_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J3",
};

HSM_State_T const CTH_State_J4 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_J4_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_J4_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J4",
};

HSM_State_T const CTH_State_C =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_C_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_C_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CTH_C_Entry,
    .Exit_Behavior = CTH_C_Exit,
    .Name = "C",
};

HSM_State_T const CTH_State_J5 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_J5_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_J5_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J5",
};

HSM_State_T const CTH_State_D =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CTH_State_D_Outbound_Transitions),
    .Outbound_Transitions = CTH_State_D_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CTH_D_Entry,
    .Exit_Behavior = CTH_D_Exit,
    .Name = "D",
};

HSM_State_T const CTH_State_J6 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CTH_State_J6_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J6",
};

HSM_T Compound_Transition_HSM =
{
    .Current_State = NULL,
    .Initial_State = &CTH_State_Initial,
};

/*--------------------------------------------------*\
 * End: Compound Transition Statemachine definition *
\*--------------------------------------------------*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
