#ifndef COMPOSITE_STATE_HSM_H
#define COMPOSITE_STATE_HSM_H
/**
 *  @file composite_state_hsm.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup composite_state_hsm_imp
 *  @{
 *
 *      API that exposes the prototypes of all Guard and Behavior functions
 *      of the Composite State HSM and the data structures used for mocking
 *      and testing.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    CSH_TRIGGER_1,
} Composite_State_HSM_Trigger_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
HSM_State_T const CSH_State_A;
HSM_State_T const CSH_State_AA;
HSM_State_T const CSH_State_AAA;
HSM_State_T const CSH_State_AAB;
HSM_State_T const CSH_State_AB;
HSM_State_T const CSH_State_ABA;
HSM_State_T const CSH_State_ABB;
HSM_State_T const CSH_State_AC;
HSM_State_T const CSH_State_B;
HSM_State_T const CSH_State_BA;
HSM_State_T const CSH_State_BAA;
HSM_State_T const CSH_State_BAB;
HSM_State_T const CSH_State_BB;
HSM_State_T const CSH_State_BBA;
HSM_State_T const CSH_State_BBB;
HSM_State_T const CSH_State_BC;
HSM_State_T const CSH_State_Initial;
HSM_State_T const CSH_State_InitialA;
HSM_State_T const CSH_State_InitialAA;
HSM_State_T const CSH_State_InitialAB;
HSM_State_T const CSH_State_InitialB;
HSM_State_T const CSH_State_InitialBA;
HSM_State_T const CSH_State_InitialBB;

HSM_Transition_T const CSH_State_A_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_AA_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_AAA_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_AAB_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_AB_Outbound_Transitions[2];
HSM_Transition_T const CSH_State_ABA_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_ABB_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_AC_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_B_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_BA_Outbound_Transitions[2];
HSM_Transition_T const CSH_State_BAA_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_BAB_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_BB_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_BBA_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_BBB_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_BC_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_Initial_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_InitialA_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_InitialAA_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_InitialAB_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_InitialB_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_InitialBA_Outbound_Transitions[1];
HSM_Transition_T const CSH_State_InitialBB_Outbound_Transitions[1];

HSM_T Composite_State_HSM;
/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/*-----------------------*\
 * State Entry functions *
\*-----------------------*/
void CSH_A_Entry(void);
void CSH_AA_Entry(void);
void CSH_AAA_Entry(void);
void CSH_AAB_Entry(void);
void CSH_AB_Entry(void);
void CSH_ABA_Entry(void);
void CSH_ABB_Entry(void);
void CSH_AC_Entry(void);
void CSH_B_Entry(void);
void CSH_BA_Entry(void);
void CSH_BAA_Entry(void);
void CSH_BAB_Entry(void);
void CSH_BB_Entry(void);
void CSH_BBA_Entry(void);
void CSH_BBB_Entry(void);
void CSH_BC_Entry(void);

/*-----------------------*\
 * State Exit functions *
\*-----------------------*/
void CSH_A_Exit(void);
void CSH_AA_Exit(void);
void CSH_AAA_Exit(void);
void CSH_AAB_Exit(void);
void CSH_AB_Exit(void);
void CSH_ABA_Exit(void);
void CSH_ABB_Exit(void);
void CSH_AC_Exit(void);
void CSH_B_Exit(void);
void CSH_BA_Exit(void);
void CSH_BAA_Exit(void);
void CSH_BAB_Exit(void);
void CSH_BB_Exit(void);
void CSH_BBA_Exit(void);
void CSH_BBB_Exit(void);
void CSH_BC_Exit(void);

/*----------------------------*\
 * Transition Guard functions *
\*----------------------------*/
bool_t CSH_A_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_AA_AB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_AAA_AAB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_AAB_AAA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_AB_AA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_ABA_ABB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_ABB_ABA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_AB_AC_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_AC_AA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_B_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_BA_BB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_BAA_BAB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_BAB_BAA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_BB_BA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_BBA_BBB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_BBB_BBA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_BA_BC_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t CSH_BC_BB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/*-----------------------------*\
 * Transition Effect functions *
\*-----------------------------*/
void CSH_A_B_Effect(void);
void CSH_AA_AB_Effect(void);
void CSH_AAA_AAB_Effect(void);
void CSH_AAB_AAA_Effect(void);
void CSH_AB_AA_Effect(void);
void CSH_ABA_ABB_Effect(void);
void CSH_ABB_ABA_Effect(void);
void CSH_AB_AC_Effect(void);
void CSH_AC_AA_Effect(void);
void CSH_B_A_Effect(void);
void CSH_BA_BB_Effect(void);
void CSH_BAA_BAB_Effect(void);
void CSH_BAB_BAA_Effect(void);
void CSH_BB_BA_Effect(void);
void CSH_BBA_BBB_Effect(void);
void CSH_BBB_BBA_Effect(void);
void CSH_BA_BC_Effect(void);
void CSH_BC_BB_Effect(void);
void CSH_Initial_Effect(void);
void CSH_InitialA_Effect(void);
void CSH_InitialAA_Effect(void);
void CSH_InitialAB_Effect(void);
void CSH_InitialB_Effect(void);
void CSH_InitialBA_Effect(void);
void CSH_InitialBB_Effect(void);

/** @} doxygen end group */
#endif /* COMPOSITE_STATE_HSM_H */
