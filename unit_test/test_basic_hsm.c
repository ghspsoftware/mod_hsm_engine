/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "basic_test_hsm.h"
#include "hsm_engine.h"

/* MOCKS */
#include "mock_basic_test_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_branching_transition_hsm.h"
#include "mock_branching_transition_composite_state_hsm.h"
#include "mock_composite_state_hsm.h"
#include "mock_compound_transition_hsm.h"
#include "mock_else_guards_hsm.h"
#include "mock_internal_transitions_hsm.h"
#include "mock_multi_transition_hsm.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the Statemachine engine will take the only transition
 * out of the initial state and transition to the target state, executing the
 * the Initial Effect and the Target State's entry.
 */
void test_HSM_Start_Enables_initial_transition_and_Enters_A(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Basic_Test_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BTH_Initial_Effect_Expect();
    BTH_A_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Basic_Test_HSM);
    
    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BTH_State_A, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_A_B_TRIGGER_Guard_true_in_A_traverses_A_B_Transition(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_A;

    /* Setup expected call chain */
    BTH_A_B_Guard_ExpectAndReturn(BTH_A_B_TRIGGER, 0, true);
    BTH_A_Exit_Expect();
    BTH_A_B_Effect_Expect();
    BTH_B_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_A_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_B, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_A_B_TRIGGER_Guard_false_in_A_stays_in_A(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_A;

    /* Setup expected call chain */
    BTH_A_B_Guard_ExpectAndReturn(BTH_A_B_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_A_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_A, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_B_C_TRIGGER_in_A_stays_in_A(void)
{

    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_A;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_B_C_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_A, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_C_A_TRIGGER_in_A_stays_in_A(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_A;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_C_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_A, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_B_C_TRIGGER_Guard_True_in_B_traverses_B_C_Transition(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_B;

    /* Setup expected call chain */
    BTH_B_C_Guard_ExpectAndReturn(BTH_B_C_TRIGGER, 0, true);
    BTH_B_Exit_Expect();
    BTH_B_C_Effect_Expect();
    BTH_C_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_B_C_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_C, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_B_C_TRIGGER_Guard_false_in_B_stays_in_B(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_B;

    /* Setup expected call chain */
    BTH_B_C_Guard_ExpectAndReturn(BTH_B_C_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_B_C_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_B, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_A_B_TRIGGER_in_B_stays_in_B(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_B;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_A_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_B, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_C_A_TRIGGER_in_B_stays_in_B(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_B;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_C_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_B, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_C_A_TRIGGER_Guard_True_in_C_traverses_C_A_Transition(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_C;

    /* Setup expected call chain */
    BTH_C_A_Guard_ExpectAndReturn(BTH_C_A_TRIGGER, 0, true);
    BTH_C_Exit_Expect();
    BTH_C_A_Effect_Expect();
    BTH_A_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_C_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_A, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_C_A_TRIGGER_Guard_false_in_C_stays_in_C(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_C;

    /* Setup expected call chain */
    BTH_C_A_Guard_ExpectAndReturn(BTH_C_A_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_C_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_C, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_A_B_TRIGGER_in_C_stays_in_C(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_C;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_A_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_C, Basic_Test_HSM.Current_State);
}

void test_HSM_Process_Event_B_C_TRIGGER_in_C_stays_in_C(void)
{
    /* Ensure known test state */
    Basic_Test_HSM.Current_State = &BTH_State_C;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Basic_Test_HSM, BTH_B_C_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BTH_State_C, Basic_Test_HSM.Current_State);
}
