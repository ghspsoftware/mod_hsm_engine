/**
 *  @file multi_transition_hsm.c
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "hsm_engine.h"
#include "multi_transition_hsm.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*-------------------------------------------------*\
 * Begin: Multi-Transition Statemachine definition *
\*-------------------------------------------------*/
HSM_Transition_T const MTH_A_Outbound_Transitions[5] =
{
    {
        .Trigger = MTH_LEAVE_A_TRIGGER,
        .Guard = MTH_A_B_Guard,
        .Effect = MTH_A_B_Effect,
        .Target = &MTH_State_B,
    },
    {
        .Trigger = MTH_LEAVE_A_TRIGGER,
        .Guard = MTH_A_C_Guard,
        .Effect = MTH_A_C_Effect,
        .Target = &MTH_State_C,
    },
    {
        .Trigger = MTH_A_B_NE_TRIGGER,
        .Guard = MTH_A_B_Guard,
        .Effect = NULL,
        .Target = &MTH_State_B,
    },
    {
        .Trigger = MTH_A_B_NG_TRIGGER,
        .Guard = NULL,
        .Effect = MTH_A_B_Effect,
        .Target = &MTH_State_B,
    },
    {
        .Trigger = MTH_A_B_NG_NE_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Target = &MTH_State_B,
    },
};

HSM_Transition_T const MTH_B_Outbound_Transitions[2] =
{
    {
        .Trigger = MTH_LEAVE_B_TRIGGER,
        .Guard = MTH_B_A_Guard,
        .Effect = MTH_B_A_Effect,
        .Target = &MTH_State_A,
    },
    {
        .Trigger = MTH_LEAVE_B_TRIGGER,
        .Guard = MTH_B_C_Guard,
        .Effect = MTH_B_C_Effect,
        .Target = &MTH_State_C,
    },
};

HSM_Transition_T const MTH_C_Outbound_Transitions[3] =
{
    {
        .Trigger = MTH_LEAVE_C_TRIGGER,
        .Guard = MTH_C_A_Guard,
        .Effect = MTH_C_A_Effect,
        .Target = &MTH_State_A,
    },
    {
        .Trigger = MTH_LEAVE_C_TRIGGER,
        .Guard = MTH_C_B_Guard,
        .Effect = MTH_C_B_Effect,
        .Target = &MTH_State_B,
    },
    {
        .Trigger = MTH_LEAVE_C_TRIGGER,
        .Guard = MTH_C_C_Guard,
        .Effect = MTH_C_C_Effect,
        .Target = &MTH_State_C,
    },
};

HSM_Transition_T const MTH_State_Initial_Outbound_Transitions[1] =
{
    {
         .Trigger = HSM_NO_TRIGGER,
         .Guard = NULL,
         .Effect = NULL,
         .Target = &MTH_State_A,
    },
};

HSM_State_T const MTH_State_A =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(MTH_A_Outbound_Transitions),
    .Outbound_Transitions = MTH_A_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = MTH_A_Entry,
    .Exit_Behavior = MTH_A_Exit,
    .Name = "A",
};

HSM_State_T const MTH_State_B =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(MTH_B_Outbound_Transitions),
    .Outbound_Transitions = MTH_B_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = MTH_B_Entry,
    .Exit_Behavior = MTH_B_Exit,
    .Name = "B",
};

HSM_State_T const MTH_State_C =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(MTH_C_Outbound_Transitions),
    .Outbound_Transitions = MTH_C_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = MTH_C_Entry,
    .Exit_Behavior = MTH_C_Exit,
    .Name = "C",
};

HSM_State_T const MTH_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = MTH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_T Multi_Transition_HSM =
{
    .Current_State = NULL,
    .Initial_State = &MTH_State_Initial,
};

/*-----------------------------------------------*\
 * End: Multi-Transition Statemachine definition *
\*-----------------------------------------------*/

/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
