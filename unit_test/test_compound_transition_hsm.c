/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "compound_transition_hsm.h"
#include "hsm_engine.h"

/* MOCKS */
#include "mock_compound_transition_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_basic_test_hsm.h"
#include "mock_branching_transition_hsm.h"
#include "mock_branching_transition_composite_state_hsm.h"
#include "mock_composite_state_hsm.h"
#include "mock_else_guards_hsm.h"
#include "mock_internal_transitions_hsm.h"
#include "mock_multi_transition_hsm.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the Statemachine engine will take the only transition
 * out of the initial state and transition to the target state, executing the
 * the Initial Effect and the Target State's entry.
 */
void test_HSM_Start_Enables_initial_transition_and_Enters_A(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Compound_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    CTH_Initial_Effect_Expect();
    CTH_J1_A_Effect_Expect();
    CTH_A_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Compound_Transition_HSM);
    
    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&CTH_State_A, Compound_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_A_TRIGGER_A_J2_Guard_true_J2_B_Guard_true_in_A_traverses_Transition(void)
{
    /* Ensure known test state */
    Compound_Transition_HSM.Current_State = &CTH_State_A;

    /* Setup expected call chain */
    CTH_A_J2_Guard_ExpectAndReturn(CTH_LEAVE_A_TRIGGER, 0, true);
    CTH_J2_B_Guard_ExpectAndReturn(CTH_LEAVE_A_TRIGGER, 0, true);
    CTH_A_Exit_Expect();
    CTH_A_J2_Effect_Expect();
    CTH_J2_B_Effect_Expect();
    CTH_B_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Compound_Transition_HSM, CTH_LEAVE_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CTH_State_B, Compound_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_A_TRIGGER_A_J2_Guard_false_in_A_stays_in_A(void)
{
    /* Ensure known test state */
    Compound_Transition_HSM.Current_State = &CTH_State_A;

    /* Setup expected call chain */
    CTH_A_J2_Guard_ExpectAndReturn(CTH_LEAVE_A_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Compound_Transition_HSM, CTH_LEAVE_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CTH_State_A, Compound_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_A_TRIGGER_A_J2_Guard_true_J2_B_Guard_false_in_A_stays_in_A(void)
{
    /* Ensure known test state */
    Compound_Transition_HSM.Current_State = &CTH_State_A;

    /* Setup expected call chain */
    CTH_A_J2_Guard_ExpectAndReturn(CTH_LEAVE_A_TRIGGER, 0, true);
    CTH_J2_B_Guard_ExpectAndReturn(CTH_LEAVE_A_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Compound_Transition_HSM, CTH_LEAVE_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CTH_State_A, Compound_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_B_TRIGGER_in_A_stays_in_A(void)
{

    /* Ensure known test state */
    Compound_Transition_HSM.Current_State = &CTH_State_A;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Compound_Transition_HSM, CTH_LEAVE_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CTH_State_A, Compound_Transition_HSM.Current_State);
}
