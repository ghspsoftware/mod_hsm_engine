/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "basic_test_hsm.h"
#include "else_guards_hsm.h"
#include "hsm_engine.h"

/* MOCKS */
#include "mock_else_guards_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_basic_test_hsm.h"
#include "mock_branching_transition_hsm.h"
#include "mock_branching_transition_composite_state_hsm.h"
#include "mock_composite_state_hsm.h"
#include "mock_compound_transition_hsm.h"
#include "mock_internal_transitions_hsm.h"
#include "mock_multi_transition_hsm.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the Statemachine engine will transition from
 * Initial to C with the following guard conditions:
 *
 * J1_J2_Guard is true
 * J2_A_Guard is false
 * J2_B_Guard is false
 */
void test_HSM_Start_Transitions_To_C_Using_else_Guard_At_J2(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Else_Guard_HSM.Current_State = NULL;

    /* Setup expected call chain */
    EGH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    EGH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    EGH_J2_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = HSM_Start(&Else_Guard_HSM);
    
    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&EGH_State_C, Else_Guard_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will transition from
 * Initial to C with the following guard conditions:
 *
 * J1_J2_Guard is false
 * J1_J3_Guard is true
 * J3_D_Guard is false
 * J3_E_Guard is false
 */
void test_HSM_Start_Transitions_To_C_Using_else_Guard_At_J3(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Else_Guard_HSM.Current_State = NULL;

    /* Setup expected call chain */
    EGH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    EGH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    EGH_J3_D_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    EGH_J3_E_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = HSM_Start(&Else_Guard_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&EGH_State_C, Else_Guard_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will transition from
 * Initial to F with the following guard conditions:
 *
 * J1_J2_Guard is false
 * J1_J3_Guard is false
 * J4_E_Guard is false
 * J4_G_Guard is false
 */
void test_HSM_Start_Transitions_To_F_Using_else_Guard_At_J1_And_At_J4(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Else_Guard_HSM.Current_State = NULL;

    /* Setup expected call chain */
    EGH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    EGH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    EGH_J4_E_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    EGH_J4_G_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = HSM_Start(&Else_Guard_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&EGH_State_F, Else_Guard_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will transition from
 * A to C when it received the EGH_EXIT_A_TRIGGER event with the
 * following guard conditions:
 *
 * A_J6_Guard is true
 * J6_A_Guard is false
 * J6_B_Guard is false
 */
void test_HSM_Process_Event_In_A_EXIT_A_TRIGGER_Transitions_To_C_Using_else_Guard_At_J6(void)
{
    /* Ensure known test state */
    Else_Guard_HSM.Current_State = &EGH_State_A;

    /* Setup expected call chain */
    EGH_A_J6_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, true);
    EGH_J6_A_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);
    EGH_J6_B_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Else_Guard_HSM, EGH_EXIT_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&EGH_State_C, Else_Guard_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will transition from
 * A to D when it received the EGH_EXIT_A_TRIGGER event with the
 * following guard conditions:
 *
 * A_J6_Guard is false
 * A_J7_Guard is true
 * J7_C_Guard is false
 * J7_E_Guard is false
 */
void test_HSM_Process_Event_In_A_EXIT_A_TRIGGER_Transitions_To_D_Using_else_Guard_At_J7(void)
{
    /* Ensure known test state */
    Else_Guard_HSM.Current_State = &EGH_State_A;

    /* Setup expected call chain */
    EGH_A_J6_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);
    EGH_A_J7_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, true);
    EGH_J7_C_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);
    EGH_J7_E_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Else_Guard_HSM, EGH_EXIT_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&EGH_State_D, Else_Guard_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will transition from
 * A to E when it received the EGH_EXIT_A_TRIGGER event with the
 * following guard conditions:
 *
 * A_J6_Guard is false
 * A_J7_Guard is false
 * J8_F_Guard is false
 * J8_G_Guard is false
 */
void test_HSM_Process_Event_In_A_EXIT_A_TRIGGER_Transitions_To_D_Using_else_Guard_At_A_And_At_J8(void)
{
    /* Ensure known test state */
    Else_Guard_HSM.Current_State = &EGH_State_A;

    /* Setup expected call chain */
    EGH_A_J6_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);
    EGH_A_J7_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);
    EGH_J8_F_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);
    EGH_J8_G_Guard_ExpectAndReturn(EGH_EXIT_A_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Else_Guard_HSM, EGH_EXIT_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&EGH_State_E, Else_Guard_HSM.Current_State);
}
