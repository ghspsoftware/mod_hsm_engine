#ifndef HSM_ENGINE_CFG_H
#define HSM_ENGINE_CFG_H
/**
 *  @file hsm_engine_cfg.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *
 *  @addtogroup hsm_engine_imp_cfg hsm_engine Implementation Configuration
 *  @{
 *      Configuration of hsm_engine Implementation
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/

/*===========================================================================*
 * \#define Constants
 *===========================================================================*/
/**
 * Maximum number of transitions allowed in a chain when moving from one 
 * state to another. Any attempts at a transition longer than this number
 * will fail. 
 */
#define HSM_MAX_TRANSITION_CHAIN_LENGTH (5)

/*===========================================================================*
 * \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Custom Type Declarations
 *===========================================================================*/

/** @} doxygen end group */
#endif /* HSM_ENGINE_CFG_H */
