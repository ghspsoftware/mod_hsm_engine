#ifndef BASIC_TEST_HSM_H
#define BASIC_TEST_HSM_H
/**
 *  @file basic_test_hsm.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup basic_test_hsm_imp
 *  @{
 *
 *      API that exposes the prototypes of all Guard and Behavior functions
 *      of the Basic Test HSM and data structures used for mocking and testing.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    BTH_A_B_TRIGGER,
    BTH_B_C_TRIGGER,
    BTH_C_A_TRIGGER
} Basic_Test_HSM_Trigger_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
HSM_State_T const BTH_State_A;
HSM_State_T const BTH_State_B;
HSM_State_T const BTH_State_C;
HSM_State_T const BTH_State_Initial;

HSM_Transition_T const BTH_State_A_Outbound_Transitions[1];
HSM_Transition_T const BTH_State_B_Outbound_Transitions[1];
HSM_Transition_T const BTH_State_C_Outbound_Transitions[1];
HSM_Transition_T const BTH_State_Initial_Outbound_Transitions[1];

HSM_T Basic_Test_HSM;

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/*-----------------------*\
 * State Entry functions *
\*-----------------------*/
void BTH_A_Entry(void);
void BTH_B_Entry(void);
void BTH_C_Entry(void);

/*-----------------------*\
 * State Exit functions *
\*-----------------------*/
void BTH_A_Exit(void);
void BTH_B_Exit(void);
void BTH_C_Exit(void);

/*----------------------------*\
 * Transition Guard functions *
\*----------------------------*/
bool_t BTH_A_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTH_B_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTH_C_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/*-----------------------------*\
 * Transition Effect functions *
\*-----------------------------*/
void BTH_A_B_Effect(void);
void BTH_B_C_Effect(void);
void BTH_C_A_Effect(void);
void BTH_Initial_Effect(void);

/** @} doxygen end group */
#endif /* BASIC_TEST_HSM_H */
