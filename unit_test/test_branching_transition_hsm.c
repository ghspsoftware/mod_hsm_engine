/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "hsm_engine.h"

/* MOCKS */
#include "mock_branching_transition_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_basic_test_hsm.h"
#include "mock_branching_transition_composite_state_hsm.h"
#include "mock_composite_state_hsm.h"
#include "mock_compound_transition_hsm.h"
#include "mock_else_guards_hsm.h"
#include "mock_internal_transitions_hsm.h"
#include "mock_multi_transition_hsm.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/*------------------------------------------------------------------*\
 *                    Initial Transition test cases                 *
\*------------------------------------------------------------------*/
/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state A
 * when the J1_J2_Guard and J2_A_Guard both are true. All other branching transition guards
 * are false.
 */
void test_HSM_Start_J1_J2_G_J2_A_G_true_initial_transition_Enters_A(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J1_J3_Guard_IgnoreAndReturn(false);
    BRTH_J1_J4_Guard_IgnoreAndReturn(false);
    BRTH_J2_C_Guard_IgnoreAndReturn(false);
    BRTH_J2_J8_Guard_IgnoreAndReturn(false);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J2_Effect_Expect();
    BRTH_J2_A_Effect_Expect();
    BRTH_A_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);
    
    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_A, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state C
 * when the J1_J2_Guard and J2_C_Guard both are true. All other branching transition guards
 * are false.
 */
void test_HSM_Start_J1_J2_G_J2_C_G_true_initial_transition_Enters_C(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J1_J3_Guard_IgnoreAndReturn(false);
    BRTH_J1_J4_Guard_IgnoreAndReturn(false);
    BRTH_J2_A_Guard_IgnoreAndReturn(false);
    BRTH_J2_J8_Guard_IgnoreAndReturn(false);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J2_Effect_Expect();
    BRTH_J2_C_Effect_Expect();
    BRTH_C_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_C, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state B
 * when the J1_J2_Guard and J2_J8_Guard both are true. All other branching transition guards
 * are false.
 */
void test_HSM_Start_J1_J2_G_J2_J8_G_true_initial_transition_Enters_B(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_J8_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J1_J3_Guard_IgnoreAndReturn(false);
    BRTH_J1_J4_Guard_IgnoreAndReturn(false);
    BRTH_J2_A_Guard_IgnoreAndReturn(false);
    BRTH_J2_C_Guard_IgnoreAndReturn(false);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J2_Effect_Expect();
    BRTH_B_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_B, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state B
 * when the J1_J3_Guard and J3_B_Guard both are true. All other branching transition guards
 * are false.
 */
void test_HSM_Start_J1_J3_G_J3_B_G_true_initial_transition_Enters_B(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J3_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J1_J2_Guard_IgnoreAndReturn(false);
    BRTH_J1_J4_Guard_IgnoreAndReturn(false);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J3_Effect_Expect();
    BRTH_J3_B_Effect_Expect();
    BRTH_B_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_B, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state B
 * when the J1_J3_Guard, J3_B_Guard and J1_J2_Guard are true. All other branching transition guards
 * are false. J1_J2_Guard being true causes processing of the outbound transitions from J2 which will
 * all be false.
 */
void test_HSM_Start_J1_J2_G_true_J1_J3_G_J3_B_G_true_initial_transition_Enters_B(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_J8_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J3_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J1_J4_Guard_IgnoreAndReturn(false);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J3_Effect_Expect();
    BRTH_J3_B_Effect_Expect();
    BRTH_B_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_B, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state B
 * when the J1_J4_Guard and J4_B_Guard both are true. All other branching transition guards
 * are false.
 */
void test_HSM_Start_J1_J4_G_J4_B_G_true_initial_transition_Enters_B(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J4_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J1_J2_Guard_IgnoreAndReturn(false);
    BRTH_J1_J3_Guard_IgnoreAndReturn(false);
    BRTH_J4_J7_Guard_IgnoreAndReturn(false);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J4_Effect_Expect();
    BRTH_J4_B_Effect_Expect();
    BRTH_B_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_B, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state B
 * when the J1_J2_Guard, J1_J3_Guard, J1_J4_Guard and J4_B_Guard are true. All other branching transition guards
 * are false. J1_J2_Guard being true causes processing of the outbound transitions from J2 which will
 * all be false. J1_J3_Guard being true causes processing of the outbound transitions from J3 which will all be false.
 */
void test_HSM_Start_J1_J2_G_true_J1_J3_G_true_J1_J4_G_true_J4_B_G_true_initial_transition_Enters_B(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_J8_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J3_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J4_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J4_J7_Guard_IgnoreAndReturn(false);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J4_Effect_Expect();
    BRTH_J4_B_Effect_Expect();
    BRTH_B_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_B, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state C
 * when the J1_J4_Guard and J4_J7_Guard both are true. All other branching transition guards
 * are false.
 */
void test_HSM_Start_J1_J4_G_J4_B_G_true_initial_transition_Enters_C(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J4_J7_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J1_J2_Guard_IgnoreAndReturn(false);
    BRTH_J1_J3_Guard_IgnoreAndReturn(false);
    BRTH_J4_B_Guard_IgnoreAndReturn(false);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J4_Effect_Expect();
    BRTH_J7_C_Effect_Expect();
    BRTH_C_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_C, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state B
 * when the J1_J2_Guard, J1_J3_Guard, J1_J4_Guard and J4_J7_Guard are true. All other branching transition guards
 * are false. J1_J2_Guard being true causes processing of the outbound transitions from J2 which will
 * all be false. J1_J3_Guard being true causes processing of the outbound transitions from J3 which will all be false.
 */
void test_HSM_Start_J1_J2_G_true_J1_J3_G_true_J1_J4_G_true_J4_J7_G_true_initial_transition_Enters_C(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_J8_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J3_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J4_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J4_J7_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    BRTH_Initial_Effect_Expect();
    BRTH_J1_J4_Effect_Expect();
    BRTH_J7_C_Effect_Expect();
    BRTH_C_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_C, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state B
 * when the J1_J2_Guard, J1_J3_Guard and J1_J4_Guard are all false. This means no valid stable state was
 * found during the initial transition and the state machine was not properly started.
 */
void test_HSM_Start_all_first_level_transitions_false_fails_to_start_state_machine(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_FALSE(result);
    TEST_ASSERT_EQUAL_PTR(NULL, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state B
 * when the J1_J2_Guard, J1_J3_Guard and J1_J4_Guard are all true but all other outbound
 * transitions are false. This means no valid stable state was found during the initial
 * transition and the state machine was not properly started.
 */
void test_HSM_Start_all_second_level_transitions_false_fails_to_start_state_machine(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_J8_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J3_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J4_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J4_J7_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = HSM_Start(&Branching_Transition_HSM);

    /* Verify test results */
    TEST_ASSERT_FALSE(result);
    TEST_ASSERT_EQUAL_PTR(NULL, Branching_Transition_HSM.Current_State);
}

/*------------------------------------------------------------------*\
 *                    Triggered Transition test cases               *
\*------------------------------------------------------------------*/

/**
 * @test Verifies that the Statemachine engine will transitions from State A
 * to State B when it processes the A_B_TRIGGER when the A_B_Guard is true
 */
void test_HSM_Process_Event_A_B_TRIGGER_A_B_G_true_Transition_To_B(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_A;

    /* Setup expected call chain */
    BRTH_A_B_Guard_ExpectAndReturn(BRTH_A_B_TRIGGER, 0, true);

    BRTH_A_Exit_Expect();
    BRTH_A_B_Effect_Expect();
    BRTH_B_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_A_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_B, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will process the A_J5_TRIGGER
 * and find a valid path from A to back to A when A_J5_Guard and J5_A_Guard are
 * all true. All other associated outbound transitions are false
 */
void test_HSM_Process_Event_A_J5_TRIGGER_A_J5_G_true_J5_A_G_true_Transition_To_A(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_A;

    /* Setup expected call chain */
    BRTH_A_J5_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J5_A_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J5_J6_Guard_IgnoreAndReturn(false);
    BRTH_J5_J9_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);

    BRTH_A_Exit_Expect();
    BRTH_A_J5_Effect_Expect();
    BRTH_J5_A_Effect_Expect();
    BRTH_A_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_A_J5_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_A, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will process the A_J5_TRIGGER
 * and find a valid path from A back to A when A_J5_Guard, J5_J6_Guard, and
 * J6_A_Guard are all true. All other associated outbound transitions are
 * false
 */
void test_HSM_Process_Event_A_J5_TRIGGER_A_J5_G_true_J5_J6_G_true_J6_A_G_true_Transition_To_B(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_A;

    /* Setup expected call chain */
    BRTH_A_J5_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J5_J6_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J6_A_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J9_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);
    BRTH_J6_B_Guard_IgnoreAndReturn(false);
    BRTH_J6_C_Guard_IgnoreAndReturn(false);
    BRTH_J6_D_Guard_IgnoreAndReturn(false);

    BRTH_A_Exit_Expect();
    BRTH_A_J5_Effect_Expect();
    BRTH_J5_J6_Effect_Expect();
    BRTH_J6_A_Effect_Expect();
    BRTH_A_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_A_J5_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_A, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will process the A_J5_TRIGGER
 * and find a valid path from A to B when A_J5_Guard, J5_J6_Guard, and
 * J6_B_Guard are all true. All other associated outbound transitions are
 * false
 */
void test_HSM_Process_Event_A_J5_TRIGGER_A_J5_G_true_J5_J6_G_true_J6_B_G_true_Transition_To_B(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_A;

    /* Setup expected call chain */
    BRTH_A_J5_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J5_J6_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J6_B_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J9_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);
    BRTH_J6_A_Guard_IgnoreAndReturn(false);
    BRTH_J6_C_Guard_IgnoreAndReturn(false);
    BRTH_J6_D_Guard_IgnoreAndReturn(false);

    BRTH_A_Exit_Expect();
    BRTH_A_J5_Effect_Expect();
    BRTH_J5_J6_Effect_Expect();
    BRTH_J6_B_Effect_Expect();
    BRTH_B_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_A_J5_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_B, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will process the A_J5_TRIGGER
 * and find a valid path from A to C when A_J5_Guard, J5_J6_Guard, and
 * J6_C_Guard are all true. All other associated outbound transitions are
 * false
 */
void test_HSM_Process_Event_A_J5_TRIGGER_A_J5_G_true_J5_J6_G_true_J6_D_G_true_Transition_To_C(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_A;

    /* Setup expected call chain */
    BRTH_A_J5_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J5_J6_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J6_C_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J9_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);
    BRTH_J6_A_Guard_IgnoreAndReturn(false);
    BRTH_J6_B_Guard_IgnoreAndReturn(false);
    BRTH_J6_D_Guard_IgnoreAndReturn(false);

    BRTH_A_Exit_Expect();
    BRTH_A_J5_Effect_Expect();
    BRTH_J5_J6_Effect_Expect();
    BRTH_J6_C_Effect_Expect();
    BRTH_C_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_A_J5_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_C, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will process the A_J5_TRIGGER
 * and find a valid path from A to D when A_J5_Guard, J5_J6_Guard, and
 * J6_D_Guard are all true. All other associated outbound transitions are
 * false
 */
void test_HSM_Process_Event_A_J5_TRIGGER_A_J5_G_true_J5_J6_G_true_J6_D_G_true_Transition_To_D(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_A;

    /* Setup expected call chain */
    BRTH_A_J5_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J5_J6_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J6_D_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J9_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);
    BRTH_J6_A_Guard_IgnoreAndReturn(false);
    BRTH_J6_B_Guard_IgnoreAndReturn(false);
    BRTH_J6_C_Guard_IgnoreAndReturn(false);

    BRTH_A_Exit_Expect();
    BRTH_A_J5_Effect_Expect();
    BRTH_J5_J6_Effect_Expect();
    BRTH_D_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_A_J5_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_D, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will process the A_J5_TRIGGER
 * and find a valid path from A to C when A_J5_Guard and J5_J9_Guard are all true.
 * All other associated outbound transitions are false.
 */
void test_HSM_Process_Event_A_J5_TRIGGER_A_J5_G_true_J5_J9_G_true_Transition_To_C(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_A;

    /* Setup expected call chain */
    BRTH_A_J5_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J5_J9_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J6_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);

    BRTH_A_Exit_Expect();
    BRTH_A_J5_Effect_Expect();
    BRTH_J9_C_Effect_Expect();
    BRTH_C_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_A_J5_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_C, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will process the A_J5_TRIGGER
 * and find a valid path from A to C when A_J5_Guard, J5_J6_Guard, J5_J10_Guard are all true.
 * All outbound guards from J6 will be false and all other outbound guards from J5 will be false.
 */
void test_HSM_Process_Event_A_J5_TRIGGER_A_J5_G_true_J5_J6_G_true_J5_J10_G_true_J10_C_G_true_Transition_To_C(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_A;

    /* Setup expected call chain */
    BRTH_A_J5_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J5_J6_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J5_J10_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);
    BRTH_J6_A_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, false);
    BRTH_J6_B_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, false);
    BRTH_J6_C_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, false);
    BRTH_J6_D_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, false);
    BRTH_J10_C_Guard_ExpectAndReturn(BRTH_A_J5_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J9_Guard_IgnoreAndReturn(false);

    BRTH_A_Exit_Expect();
    BRTH_A_J5_Effect_Expect();
    BRTH_J5_J10_Effect_Expect();
    BRTH_J10_C_Effect_Expect();
    BRTH_C_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_A_J5_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_C, Branching_Transition_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will process the D_J5_TRIGGER
 * and find a valid path from D to A when D_J5_Guard, J5_J6_Guard, J6_A_Guard are all true.
 * All other guards will be false. This exercises a junction with multiple incoming transitions.
 */
void test_HSM_Process_Event_D_J5_TRIGGER_D_J5_G_true_J5_J6_G_true_J6_A_G_true_Transition_To_A(void)
{
    /* Ensure known test state */
    Branching_Transition_HSM.Current_State = &BRTH_State_D;

    /* Setup expected call chain */
    BRTH_D_J5_Guard_ExpectAndReturn(BRTH_D_J5_TRIGGER, 0, true);
    BRTH_J5_J6_Guard_ExpectAndReturn(BRTH_D_J5_TRIGGER, 0, true);
    BRTH_J6_A_Guard_ExpectAndReturn(BRTH_D_J5_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J9_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);
    BRTH_J6_B_Guard_IgnoreAndReturn(false);
    BRTH_J6_C_Guard_IgnoreAndReturn(false);
    BRTH_J6_D_Guard_IgnoreAndReturn(false);

    BRTH_D_Exit_Expect();
    BRTH_D_J5_Effect_Expect();
    BRTH_J5_J6_Effect_Expect();
    BRTH_J6_A_Effect_Expect();
    BRTH_A_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Branching_Transition_HSM, BRTH_D_J5_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_A, Branching_Transition_HSM.Current_State);
}
