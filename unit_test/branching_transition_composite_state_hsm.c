/**
 *  @file branching_transition_composite_state_hsm.c
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "branching_transition_composite_state_hsm.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*------------------------------------------------*\
 * Begin: Composite State Statemachine definition *
\*------------------------------------------------*/
HSM_Transition_T const BTCSH_State_A_Outbound_Transitions[1] =
{
    {
        .Trigger = BTCSH_A_TRIGGER,
        .Guard = BTCSH_A_J2_Guard,
        .Effect = BTCSH_A_J2_Effect,
        .Source = &BTCSH_State_A,
        .Target = &BTCSH_State_J2,
    }
};

HSM_Transition_T const BTCSH_State_AA_Outbound_Transitions[1] =
{
    {
        .Trigger = BTCSH_A_TRIGGER,
        .Guard = BTCSH_AA_AJ2_Guard,
        .Effect = BTCSH_AA_AJ2_Effect,
        .Source = &BTCSH_State_AA,
        .Target = &BTCSH_State_AJ2,
    }
};

HSM_Transition_T const BTCSH_State_AJ1_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_AJ1_AA_Guard,
        .Effect = BTCSH_AJ1_AA_Effect,
        .Source = &BTCSH_State_AJ1,
        .Target = &BTCSH_State_AA,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_AJ1_AB_Guard,
        .Effect = BTCSH_AJ1_AB_Effect,
        .Source = &BTCSH_State_AJ1,
        .Target = &BTCSH_State_AB,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_AJ1_AC_Guard,
        .Effect = BTCSH_AJ1_AC_Effect,
        .Source = &BTCSH_State_AJ1,
        .Target = &BTCSH_State_AC,
    }
};

HSM_Transition_T const BTCSH_State_AJ2_Outbound_Transitions[2] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_AJ2_AB_Guard,
        .Effect = BTCSH_AJ2_AB_Effect,
        .Source = &BTCSH_State_AJ2,
        .Target = &BTCSH_State_AB,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_AJ2_AC_Guard,
        .Effect = BTCSH_AJ2_AC_Effect,
        .Source = &BTCSH_State_AJ2,
        .Target = &BTCSH_State_AC,
    }
};

HSM_Transition_T const BTCSH_State_BJ1_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_BJ1_BA_Guard,
        .Effect = BTCSH_BJ1_BA_Effect,
        .Source = &BTCSH_State_BJ1,
        .Target = &BTCSH_State_BA,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_BJ1_BB_Guard,
        .Effect = BTCSH_BJ1_BB_Effect,
        .Source = &BTCSH_State_BJ1,
        .Target = &BTCSH_State_BB,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_BJ1_BC_Guard,
        .Effect = BTCSH_BJ1_BC_Effect,
        .Source = &BTCSH_State_BJ1,
        .Target = &BTCSH_State_BC,
    }
};

HSM_Transition_T const BTCSH_State_CJ1_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_CJ1_CA_Guard,
        .Effect = BTCSH_CJ1_CA_Effect,
        .Source = &BTCSH_State_CJ1,
        .Target = &BTCSH_State_CA,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_CJ1_CB_Guard,
        .Effect = BTCSH_CJ1_CB_Effect,
        .Source = &BTCSH_State_CJ1,
        .Target = &BTCSH_State_CB,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_CJ1_CC_Guard,
        .Effect = BTCSH_CJ1_CC_Effect,
        .Source = &BTCSH_State_CJ1,
        .Target = &BTCSH_State_CC,
    }
};

HSM_Transition_T const BTCSH_State_Initial_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = BTCSH_Initial_Effect,
        .Source = &BTCSH_State_Initial,
        .Target = &BTCSH_State_J1,
    }
};

HSM_Transition_T const BTCSH_State_InitialA_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = BTCSH_InitialA_Effect,
        .Source = &BTCSH_State_InitialA,
        .Target = &BTCSH_State_AJ1,
    }
};

HSM_Transition_T const BTCSH_State_InitialB_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = BTCSH_InitialB_Effect,
        .Source = &BTCSH_State_InitialB,
        .Target = &BTCSH_State_BJ1,
    }
};

HSM_Transition_T const BTCSH_State_InitialC_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = BTCSH_InitialC_Effect,
        .Source = &BTCSH_State_InitialC,
        .Target = &BTCSH_State_CJ1,
    }
};

HSM_Transition_T const BTCSH_State_J1_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_J1_A_Guard,
        .Effect = BTCSH_J1_A_Effect,
        .Source = &BTCSH_State_J1,
        .Target = &BTCSH_State_A,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_J1_B_Guard,
        .Effect = BTCSH_J1_B_Effect,
        .Source = &BTCSH_State_J1,
        .Target = &BTCSH_State_B,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_J1_C_Guard,
        .Effect = BTCSH_J1_C_Effect,
        .Source = &BTCSH_State_J1,
        .Target = &BTCSH_State_C,
    }
};

HSM_Transition_T const BTCSH_State_J2_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_J2_A_Guard,
        .Effect = BTCSH_J2_A_Effect,
        .Source = &BTCSH_State_J2,
        .Target = &BTCSH_State_A,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_J2_B_Guard,
        .Effect = BTCSH_J2_B_Effect,
        .Source = &BTCSH_State_J2,
        .Target = &BTCSH_State_B,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BTCSH_J2_C_Guard,
        .Effect = BTCSH_J2_C_Effect,
        .Source = &BTCSH_State_J2,
        .Target = &BTCSH_State_C,
    }
};

HSM_State_T const BTCSH_State_A =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &BTCSH_State_InitialA,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTCSH_State_A_Outbound_Transitions),
    .Outbound_Transitions = BTCSH_State_A_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_A_Entry,
    .Exit_Behavior = BTCSH_A_Exit,
    .Name = "A",
};

HSM_State_T const BTCSH_State_AA =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_A,
    .Num_Outbound_Transitions = Num_Elems(BTCSH_State_AA_Outbound_Transitions),
    .Outbound_Transitions = BTCSH_State_AA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_AA_Entry,
    .Exit_Behavior = BTCSH_AA_Exit,
    .Name = "AA",
};

HSM_State_T const BTCSH_State_AB =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_A,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_AB_Entry,
    .Exit_Behavior = NULL,
    .Name = "AB",
};

HSM_State_T const BTCSH_State_AC =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_A,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_AC_Entry,
    .Exit_Behavior = NULL,
    .Name = "AC",
};

HSM_State_T const BTCSH_State_AJ1 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTCSH_State_AJ1_Outbound_Transitions),
    .Outbound_Transitions = BTCSH_State_AJ1_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const BTCSH_State_AJ2 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTCSH_State_AJ2_Outbound_Transitions),
    .Outbound_Transitions = BTCSH_State_AJ2_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const BTCSH_State_B =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &BTCSH_State_InitialB,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_B_Entry,
    .Exit_Behavior = NULL,
    .Name = "B",
};

HSM_State_T const BTCSH_State_BA =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_B,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_BA_Entry,
    .Exit_Behavior = NULL,
    .Name = "BA",
};

HSM_State_T const BTCSH_State_BB =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_B,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_BB_Entry,
    .Exit_Behavior = NULL,
    .Name = "BB",
};

HSM_State_T const BTCSH_State_BC =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_B,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_BC_Entry,
    .Exit_Behavior = NULL,
    .Name = "BC",
};

HSM_State_T const BTCSH_State_BJ1 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTCSH_State_BJ1_Outbound_Transitions),
    .Outbound_Transitions = BTCSH_State_BJ1_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const BTCSH_State_C =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &BTCSH_State_InitialC,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_C_Entry,
    .Exit_Behavior = NULL,
    .Name = "C",
};

HSM_State_T const BTCSH_State_CA =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_C,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_CA_Entry,
    .Exit_Behavior = NULL,
    .Name = "CA",
};

HSM_State_T const BTCSH_State_CB =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_C,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_CB_Entry,
    .Exit_Behavior = NULL,
    .Name = "CB",
};

HSM_State_T const BTCSH_State_CC =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_C,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTCSH_CC_Entry,
    .Exit_Behavior = NULL,
    .Name = "CC",
};

HSM_State_T const BTCSH_State_CJ1 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTCSH_State_CJ1_Outbound_Transitions),
    .Outbound_Transitions = BTCSH_State_CJ1_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const BTCSH_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = BTCSH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const BTCSH_State_InitialA =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_A,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = BTCSH_State_InitialA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const BTCSH_State_InitialB =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_B,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = BTCSH_State_InitialB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const BTCSH_State_InitialC =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &BTCSH_State_C,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = BTCSH_State_InitialC_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const BTCSH_State_J1 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTCSH_State_J1_Outbound_Transitions),
    .Outbound_Transitions = BTCSH_State_J1_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const BTCSH_State_J2 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTCSH_State_J2_Outbound_Transitions),
    .Outbound_Transitions = BTCSH_State_J2_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_T Branching_Composite_HSM =
{
    .Current_State = NULL,
    .Initial_State = &BTCSH_State_Initial,
};

/*----------------------------------------------*\
 * End: Composite State Statemachine definition *
\*----------------------------------------------*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
