/**
 *  @file basic_test_hsm.c
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "else_guards_hsm.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*-------------------------------------------*\
 * Begin: Basic Test Statemachine definition *
\*-------------------------------------------*/

HSM_Transition_T const EGH_State_A_Outbound_Transitions[3] =
{
    {
        .Trigger = EGH_EXIT_A_TRIGGER,
        .Guard = EGH_A_J6_Guard,
        .Effect = NULL,
        .Source = &EGH_State_A,
        .Target = &EGH_State_J6,
    },
    {
        .Trigger = EGH_EXIT_A_TRIGGER,
        .Guard = EGH_A_J7_Guard,
        .Effect = NULL,
        .Source = &EGH_State_A,
        .Target = &EGH_State_J7,
    },
    {
        .Trigger = EGH_EXIT_A_TRIGGER,
        .Guard = NULL, /* else */
        .Effect = NULL,
        .Source = &EGH_State_A,
        .Target = &EGH_State_J8,
    },
};

HSM_Transition_T const EGH_State_J1_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J1_J2_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J1,
        .Target = &EGH_State_J2,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J1_J3_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J1,
        .Target = &EGH_State_J3,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL, /* else */
        .Effect = NULL,
        .Source = &EGH_State_J1,
        .Target = &EGH_State_J4,
    },
};

HSM_Transition_T const EGH_State_J2_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J2_A_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J2,
        .Target = &EGH_State_A,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J2_B_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J2,
        .Target = &EGH_State_B,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL, /* else */
        .Effect = NULL,
        .Source = &EGH_State_J2,
        .Target = &EGH_State_C,
    },
};

HSM_Transition_T const EGH_State_J3_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J3_D_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J3,
        .Target = &EGH_State_D,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J3_E_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J3,
        .Target = &EGH_State_E,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL, /* else */
        .Effect = NULL,
        .Source = &EGH_State_J3,
        .Target = &EGH_State_C,
    },
};

HSM_Transition_T const EGH_State_J4_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J4_E_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J4,
        .Target = &EGH_State_E,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J4_G_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J4,
        .Target = &EGH_State_G,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL, /* else */
        .Effect = NULL,
        .Source = &EGH_State_J4,
        .Target = &EGH_State_F,
    },
};

HSM_Transition_T const EGH_State_J6_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J6_A_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J6,
        .Target = &EGH_State_A,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J6_B_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J6,
        .Target = &EGH_State_B,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL, /* else */
        .Effect = NULL,
        .Source = &EGH_State_J6,
        .Target = &EGH_State_C,
    },
};

HSM_Transition_T const EGH_State_J7_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J7_C_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J7,
        .Target = &EGH_State_C,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J7_E_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J7,
        .Target = &EGH_State_E,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL, /* else */
        .Effect = NULL,
        .Source = &EGH_State_J7,
        .Target = &EGH_State_D,
    },
};

HSM_Transition_T const EGH_State_J8_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J8_F_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J8,
        .Target = &EGH_State_F,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = EGH_J8_G_Guard,
        .Effect = NULL,
        .Source = &EGH_State_J8,
        .Target = &EGH_State_G,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL, /* else */
        .Effect = NULL,
        .Source = &EGH_State_J8,
        .Target = &EGH_State_E,
    },
};

HSM_Transition_T const EGH_State_Initial_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &EGH_State_Initial,
        .Target = &EGH_State_J1,
    },
};

HSM_State_T const EGH_State_A =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(EGH_State_A_Outbound_Transitions),
    .Outbound_Transitions = EGH_State_A_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "A",
};

HSM_State_T const EGH_State_B =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "B",
};

HSM_State_T const EGH_State_C =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "C",
};

HSM_State_T const EGH_State_D =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "D",
};

HSM_State_T const EGH_State_E =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "E",
};

HSM_State_T const EGH_State_F =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "F",
};

HSM_State_T const EGH_State_G =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "G",
};

HSM_State_T const EGH_State_J1 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(EGH_State_J1_Outbound_Transitions),
    .Outbound_Transitions = EGH_State_J1_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const EGH_State_J2 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(EGH_State_J2_Outbound_Transitions),
    .Outbound_Transitions = EGH_State_J2_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J2",
};

HSM_State_T const EGH_State_J3 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(EGH_State_J3_Outbound_Transitions),
    .Outbound_Transitions = EGH_State_J3_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J3",
};

HSM_State_T const EGH_State_J4 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(EGH_State_J4_Outbound_Transitions),
    .Outbound_Transitions = EGH_State_J4_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J4",
};

HSM_State_T const EGH_State_J6 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(EGH_State_J6_Outbound_Transitions),
    .Outbound_Transitions = EGH_State_J6_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J6",
};

HSM_State_T const EGH_State_J7 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(EGH_State_J7_Outbound_Transitions),
    .Outbound_Transitions = EGH_State_J7_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J7",
};

HSM_State_T const EGH_State_J8 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(EGH_State_J8_Outbound_Transitions),
    .Outbound_Transitions = EGH_State_J8_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J8",
};

HSM_State_T const EGH_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = EGH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_T Else_Guard_HSM =
{
    .Current_State = NULL,
    .Initial_State = &EGH_State_Initial,
};

/*-----------------------------------------*\
 * End: Basic Test Statemachine definition *
\*-----------------------------------------*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
