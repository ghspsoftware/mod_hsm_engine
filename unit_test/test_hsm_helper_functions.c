/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "basic_test_hsm.h"
#include "branching_transition_hsm.h"
#include "branching_transition_composite_state_hsm.h"
#include "composite_state_hsm.h"
#include "compound_transition_hsm.h"
#include "hsm_engine.h"

/* MOCKS */
#include "mock_basic_test_hsm.h"
#include "mock_branching_transition_hsm.h"
#include "mock_branching_transition_composite_state_hsm.h"
#include "mock_composite_state_hsm.h"
#include "mock_compound_transition_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_else_guards_hsm.h"
#include "mock_internal_transitions_hsm.h"
#include "mock_multi_transition_hsm.h"

/* STATIC's */
HSM_State_T const * hsm_Find_Least_Common_Ancestor(HSM_State_T const * State_1, HSM_State_T const * State_2);
bool_t hsm_Transition_Is_Enabled(HSM_Transition_T const * Transition, HSM_Trigger_T Event, uint32_t Event_Data);
HSM_Status_T hsm_Build_Transition(HSM_T * HSM, HSM_Transition_T const * Start_Transition, HSM_Trigger_T Event, uint32_t Event_Data);

void setUp(void)
{

}

void tearDown(void)
{

}

/*-------------------------------------------*\
 * hsm_Find_Least_Common_Ancestor Test cases *
\*-------------------------------------------*/
void test_hsm_Find_Least_Common_Ancestor_Finds_LCA_is_A_in_AAA_to_AB(void)
{
    HSM_State_T const * result_state = NULL;
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result_state = hsm_Find_Least_Common_Ancestor(&CSH_State_AAA, &CSH_State_AB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_A, result_state);
}

void test_hsm_Find_Least_Common_Ancestor_Finds_LCA_is_AA_in_AAA_to_AAB(void)
{
    HSM_State_T const * result_state = NULL;
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result_state = hsm_Find_Least_Common_Ancestor(&CSH_State_AAA, &CSH_State_AAB);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_AA, result_state);
}

void test_hsm_Find_Least_Common_Ancestor_Finds_LCA_is_Root_in_AAA_to_B(void)
{
    HSM_State_T const * result_state = NULL;
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result_state = hsm_Find_Least_Common_Ancestor(&CSH_State_AAA, &CSH_State_B);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(NULL, result_state);
}

void test_hsm_Find_Least_Common_Ancestor_Finds_LCA_is_Root_in_AA_to_B(void)
{
    HSM_State_T const * result_state = NULL;
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result_state = hsm_Find_Least_Common_Ancestor(&CSH_State_AA, &CSH_State_B);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(NULL, result_state);
}

void test_hsm_Find_Least_Common_Ancestor_Finds_LCA_is_Root_in_A_to_B(void)
{
    HSM_State_T const * result_state = NULL;
    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result_state = hsm_Find_Least_Common_Ancestor(&CSH_State_A, &CSH_State_B);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(NULL, result_state);
}

/*-------------------------------------------------*\
 * hsm_Transition_Is_Enabled Test cases            *
\*-------------------------------------------------*/

/**
 * @test Verifies that the function hsm_Transition_Is_Enabled()
 * will return true when the transition has a matching Trigger and
 * has a guard that returns true
 *
 * Statemachine: Basic Test Statemachine
 */
void test_hsm_Transition_Is_Enabled_A_B_TRIGGER_A_B_Guard_true_returns_true(void)
{
    bool_t result = false;
    HSM_Transition_T * Test_Transition = &BTH_State_A_Outbound_Transitions[0];

    /* Ensure known test state */

    /* Setup expected call chain */
    BTH_A_B_Guard_ExpectAndReturn(BTH_A_B_TRIGGER, 0, true);

    /* Call function under test */
    result = hsm_Transition_Is_Enabled(Test_Transition, BTH_A_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
}

/**
 * @test Verifies that the function hsm_Transition_Is_Enabled()
 * will return false when the transition does not have a matching Trigger.
 *
 * Statemachine: Basic Test Statemachine
 */
void test_hsm_Transition_Is_Enabled_Wrong_Trigger_returns_false(void)
{
    bool_t result = false;
    HSM_Transition_T * Test_Transition = &BTH_State_A_Outbound_Transitions[0];

    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Transition_Is_Enabled(Test_Transition, BTH_B_C_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_FALSE(result);
}

/**
 * @test Verifies that the function hsm_Transition_Is_Enabled()
 * will return true when the transition has a matching Trigger but the guard
 * returns false
 *
 * Statemachine: Basic Test Statemachine
 */
void test_hsm_Transition_Is_Enabled_A_B_TRIGGER_A_B_Guard_false_returns_false(void)
{
    bool_t result = false;
    HSM_Transition_T * Test_Transition = &BTH_State_A_Outbound_Transitions[0];

    /* Ensure known test state */

    /* Setup expected call chain */
    BTH_A_B_Guard_ExpectAndReturn(BTH_A_B_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Transition_Is_Enabled(Test_Transition, BTH_A_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_FALSE(result);
}

/**
 * @test Verifies that the function hsm_Transition_Is_Enabled()
 * will return true when the transition has no Trigger and
 * has a guard that returns true
 *
 * Statemachine: Compound Transition Statemachine
 */
void test_hsm_Transition_Is_Enabled_No_Trigger_J2_B_Guard_true_returns_true(void)
{
    bool_t result = false;
    HSM_Transition_T * Test_Transition = &CTH_State_J2_Outbound_Transitions[0];

    /* Ensure known test state */

    /* Setup expected call chain */
    CTH_J2_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Call function under test */
    result = hsm_Transition_Is_Enabled(Test_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
}

/**
 * @test Verifies that the function hsm_Transition_Is_Enabled()
 * will return false when the transition has no Trigger and
 * has a guard that returns false
 *
 * Statemachine: Compound Transition Statemachine
 */
void test_hsm_Transition_Is_Enabled_No_Trigger_J2_B_Guard_false_returns_false(void)
{
    bool_t result = false;
    HSM_Transition_T * Test_Transition = &CTH_State_J2_Outbound_Transitions[0];

    /* Ensure known test state */

    /* Setup expected call chain */
    CTH_J2_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Transition_Is_Enabled(Test_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_FALSE(result);
}

/**
 * @test Verifies that the function hsm_Transition_Is_Enabled()
 * will return true when the transition has a matching Trigger and
 * no guard
 *
 * Statemachine: Compound Transition Statemachine
 */
void test_hsm_Transition_Is_Enabled_LEAVE_B_TRIGGER_No_Guard_returns_true(void)
{
    bool_t result = false;
    HSM_Transition_T * Test_Transition = &CTH_State_B_Outbound_Transitions[0];

    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Transition_Is_Enabled(Test_Transition, CTH_LEAVE_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
}

/**
 * @test Verifies that the function hsm_Transition_Is_Enabled()
 * will return true when the transition has no Trigger and
 * no guard
 *
 * Statemachine: Compound Transition Statemachine
 */
void test_hsm_Transition_Is_Enabled_No_Trigger_No_Guard_returns_true(void)
{
    bool_t result = false;
    HSM_Transition_T * Test_Transition = &CTH_State_J6_Outbound_Transitions[0];

    /* Ensure known test state */

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Transition_Is_Enabled(Test_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_TRUE(result);
}

/*-------------------------------------------------*\
 * hsm_Build_Transition Test cases                 *
\*-------------------------------------------------*/

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * Initial -> A
 *
 * When given a start transition of Initial -> A
 *
 * Statemachine: Basic Test Statemachine
 */
void test_hsm_Build_Transition_Initial_To_A_Transition_as_Start_BTH(void)
{
    HSM_Transition_T * Start_Transition = &BTH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Build_Transition(&Basic_Test_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(1, Basic_Test_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTH_State_Initial_Outbound_Transitions[0], Basic_Test_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Basic Test Initial to A */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> B
 *
 * When given a start transition of A -> B
 *
 * Statemachine: Basic Test Statemachine
 */
void test_hsm_Build_Transition_A_to_B_Transition_as_Start_BTH(void)
{
    HSM_Transition_T * Start_Transition = &BTH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Build_Transition(&Basic_Test_HSM, Start_Transition, BTH_A_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(1, Basic_Test_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTH_State_A_Outbound_Transitions[0], Basic_Test_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Basic Test A to B */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * Initial -> J1
 * J1 -> A
 *
 * When given a start transition of Initial -> A
 *
 * Statemachine: Compound Transition Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_CTH(void)
{
    HSM_Transition_T * Start_Transition = &CTH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Build_Transition(&Compound_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(2, Compound_Transition_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&CTH_State_Initial_Outbound_Transitions[0], Compound_Transition_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Compound Transition Initial to J1 */
    TEST_ASSERT_EQUAL_PTR(&CTH_State_J1_Outbound_Transitions[0], Compound_Transition_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Compound Transition J1 to A */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * B -> J3
 * J3 -> J4
 * J4 -> C
 *
 * When given a start transition of B -> J3
 *
 * J3_J4_Guard is true
 *
 * Statemachine: Compound Transition Statemachine
 */
void test_hsm_Build_Transition_B_To_J3_Transition_As_Start_J3_J4_G_true_CTH(void)
{
    HSM_Transition_T * Start_Transition = &CTH_State_B_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    CTH_J3_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Call function under test */
    result = hsm_Build_Transition(&Compound_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(3, Compound_Transition_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&CTH_State_B_Outbound_Transitions[0], Compound_Transition_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Compound Transition B to J3 */
    TEST_ASSERT_EQUAL_PTR(&CTH_State_J3_Outbound_Transitions[0], Compound_Transition_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Compound Transition J3 to J4 */
    TEST_ASSERT_EQUAL_PTR(&CTH_State_J4_Outbound_Transitions[0], Compound_Transition_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* Compound Transition J4 to C */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition list
 *
 * When given a start transition of B -> J3
 *
 * J3_J4_Guard is false
 *
 * Statemachine: Compound Transition Statemachine
 */
void test_hsm_Build_Transition_B_To_J3_Transition_As_Start_J3_J4_G_false_CTH(void)
{
    HSM_Transition_T * Start_Transition = &CTH_State_B_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    CTH_J3_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Compound_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Compound_Transition_HSM.Transition_Chain_Data.Chain_Size);
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> J2
 * J2 -> B
 *
 * When given a start transition of A -> J2
 *
 * J2_B_Guard is true
 *
 * Statemachine: Compound Transition Statemachine
 */
void test_hsm_Build_Transition_A_To_J2_Transition_As_Start_A_J2_G_true_J2_B_Guard_true_CTH(void)
{
    HSM_Transition_T * Start_Transition = &CTH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    CTH_J2_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Call function under test */
    result = hsm_Build_Transition(&Compound_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(2, Compound_Transition_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&CTH_State_A_Outbound_Transitions[0], Compound_Transition_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Compound Transition A to J2 */
    TEST_ASSERT_EQUAL_PTR(&CTH_State_J2_Outbound_Transitions[0], Compound_Transition_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Compound Transition J2 to B */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * Initial -> J1
 * J1 -> J4
 * J4 -> J7
 * J7 -> C
 *
 * When given a start transition of Initial -> J1
 *
 * J1_J2_Guard is true
 * J2_A_Guard is false
 * J2_C_Guard is false
 * J2_J8_Guard is false
 * J1_J3_Guard is true
 * J3_B_Guard is false
 * J1_J4_Guard is true
 * J4_B_Guard is false
 * J4_J7_Guard is true
 *
 * Statemachine: Branching Transition Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_Valid_Path_To_C_BRTH(void)
{
    HSM_Transition_T * Start_Transition = &BRTH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_J8_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J3_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J4_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J4_J7_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Transition_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_Initial_Outbound_Transitions[0], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Branching Transition Initial to J1 */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_J1_Outbound_Transitions[2], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Branching Transition J1 to J4 */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_J4_Outbound_Transitions[1], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* Branching Transition J4 to J7 */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_J7_Outbound_Transitions[0], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* Branching Transition J4 to J7 */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition list
 *
 * When given a start transition of Initial -> J1
 *
 * J1_J2_Guard is false
 * J1_J3_Guard is false
 * J1_J4_Guard is false
 *
 * Statemachine: Branching Transition Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_No_Valid_Path_From_Level_1_Junctions_BRTH(void)
{
    HSM_Transition_T * Start_Transition = &BRTH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Branching_Transition_HSM.Transition_Chain_Data.Chain_Size);
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition list
 *
 * When given a start transition of Initial -> J1
 *
 * J1_J2_Guard is true
 * J2_A_Guard is false
 * J2_C_Guard is false
 * J2_J8_Guard is false
 * J1_J3_Guard is true
 * J3_B_Guard is false
 * J1_J4_Guard is true
 * J4_B_Guard is false
 * J4_J7_Guard is false
 *
 * Statemachine: Branching Transition Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_No_Valid_Path_From_Level_2_Junctions_BRTH(void)
{
    HSM_Transition_T * Start_Transition = &BRTH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BRTH_J1_J2_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J2_J8_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J3_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J3_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J1_J4_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J4_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J4_J7_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Branching_Transition_HSM.Transition_Chain_Data.Chain_Size);
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> J5
 * J5 -> J6
 * J6 -> A
 *
 * When given a start transition of A -> J5
 *
 * J5_J6_Guard is true
 * J6_A_Guard is true
 *
 * All other related guards are false
 *
 * Statemachine: Branching Transition Statemachine
 */
void test_hsm_Build_Transition_A_To_J5_Transition_As_Start_Valid_Path_To_A_BRTH(void)
{
    HSM_Transition_T * Start_Transition = &BRTH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BRTH_J5_J6_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J6_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* All other outbound transitions are false, but the order or if they are called doesn't matter */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);
    BRTH_J5_J9_Guard_IgnoreAndReturn(false);
    BRTH_J6_B_Guard_IgnoreAndReturn(false);
    BRTH_J6_C_Guard_IgnoreAndReturn(false);
    BRTH_J6_D_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(3, Branching_Transition_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_A_Outbound_Transitions[0], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Branching Transition A to J5 */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_J5_Outbound_Transitions[1], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Branching Transition J5 to J6 */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_J6_Outbound_Transitions[0], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* Branching Transition J6 to A */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> J5
 * J5 -> J9
 * J9 -> C
 *
 * When given a start transition of A -> J5
 *
 * J5_A_Guard is false
 * J5_J6_Guard is true
 * J5_J10_Guard is false
 * J6_A_Guard is false
 * J6_B_Guard is false
 * J6_C_Guard is false
 * J6_D_Guard is false
 * J6_A_Guard is true
 *
 * Statemachine: Branching Transition Statemachine
 */
void test_hsm_Build_Transition_A_To_J5_Transition_As_Start_Valid_Path_To_C_BRTH(void)
{
    HSM_Transition_T * Start_Transition = &BRTH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BRTH_J5_J6_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J6_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J6_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J6_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J6_D_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J5_J9_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* All other outbound transitions are false, but the order or if they are called doesn't matter */
    BRTH_J5_A_Guard_IgnoreAndReturn(false);
    BRTH_J5_J10_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(3, Branching_Transition_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_A_Outbound_Transitions[0], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Branching Transition A to J5 */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_J5_Outbound_Transitions[2], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Branching Transition J5 to J9 */
    TEST_ASSERT_EQUAL_PTR(&BRTH_State_J9_Outbound_Transitions[0], Branching_Transition_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* Branching Transition J9 to C */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition list
 *
 * When given a start transition of A -> J5
 *
 * J5_A_Guard is false
 * J5_J6_Guard is false
 * J5_J9_Guard is false
 * J5_J10_Guard is false
 *
 * Statemachine: Branching Transition Statemachine
 */
void test_hsm_Build_Transition_A_To_J5_Transition_As_Start_No_Valid_Path_From_Level_1_Junctions_BRTH(void)
{
    HSM_Transition_T * Start_Transition = &BRTH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BRTH_J5_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J5_J6_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J5_J9_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J5_J10_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Branching_Transition_HSM.Transition_Chain_Data.Chain_Size);
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition list
 *
 * When given a start transition of A -> J5
 *
 * J5_A_Guard is false
 * J5_J6_Guard is true
 * J6_A_Guard is false
 * J6_B_Guard is false
 * J6_C_Guard is false
 * J6_D_Guard is false
 * J5_J9_Guard is false
 * J5_J10_Guard is true
 * J10_C_Guard is false
 *
 * Statemachine: Branching Transition Statemachine
 */
void test_hsm_Build_Transition_A_To_J5_Transition_As_Start_No_Valid_Path_From_Level_2_Junctions_BRTH(void)
{
    HSM_Transition_T * Start_Transition = &BRTH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BRTH_J5_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J5_J6_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J6_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J6_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J6_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J6_D_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J5_J9_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BRTH_J5_J10_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BRTH_J10_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Transition_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Branching_Transition_HSM.Transition_Chain_Data.Chain_Size);
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * Initial -> A
 * InitialA -> AA
 * InitialAA -> AAA
 *
 * When given a start transition of Initial -> A
 *
 * Statemachine: Composite State Statemachine
 */
void test_hsm_Build_Transition_Initial_To_A_Transition_As_Start_CSH(void)
{
    HSM_Transition_T * Start_Transition = &CSH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Build_Transition(&Composite_State_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(3, Composite_State_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&CSH_State_Initial_Outbound_Transitions[0], Composite_State_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Composite State Initial to A */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_InitialA_Outbound_Transitions[0], Composite_State_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Composite State InitialA to AA */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_InitialAA_Outbound_Transitions[0], Composite_State_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* Composite State InitialAA to AAA */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * AA -> AB
 * InitialAB -> ABA
 *
 * When given a start transition of AA -> AB
 *
 * Statemachine: Composite State Statemachine
 */
void test_hsm_Build_Transition_AA_To_AB_Transition_As_Start_CSH(void)
{
    HSM_Transition_T * Start_Transition = &CSH_State_AA_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Build_Transition(&Composite_State_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(2, Composite_State_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&CSH_State_AA_Outbound_Transitions[0], Composite_State_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Composite State AA to AB */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_InitialAB_Outbound_Transitions[0], Composite_State_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Composite State InitialAB to ABA */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> B
 * InitialB -> BA
 * InitialBA -> BAA
 *
 * When given a start transition of A -> B
 *
 * Statemachine: Composite State Statemachine
 */
void test_hsm_Build_Transition_A_To_B_Transition_As_Start_CSH(void)
{
    HSM_Transition_T * Start_Transition = &CSH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Build_Transition(&Composite_State_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(3, Composite_State_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&CSH_State_A_Outbound_Transitions[0], Composite_State_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Composite State A to B */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_InitialB_Outbound_Transitions[0], Composite_State_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* Composite State InitialB to BA */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_InitialBA_Outbound_Transitions[0], Composite_State_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* Composite State InitialBA to BAA */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * Initial -> J1
 * J1 -> A
 * InitialA -> AJ1
 * AJ1 -> AA
 *
 * And the following Guards:
 *
 * J1_A_Guard is true
 * AJ1_AA_Guard is true
 * All other guards are false
 *
 * When given a start transition of Initial -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_to_AA_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J1_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    BTCSH_J1_B_Guard_IgnoreAndReturn(false);
    BTCSH_J1_C_Guard_IgnoreAndReturn(false);
    BTCSH_AJ1_AB_Guard_IgnoreAndReturn(false);
    BTCSH_AJ1_AC_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_Initial_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Initial to J1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_J1_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* J1 to A */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_InitialA_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* InitialA to AJ1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_AJ1_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* AJ1 to AA */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * Initial -> J1
 * J1 -> A
 * InitialA -> AJ1
 * AJ1 -> AB
 *
 * And the following Guards:
 *
 * J1_A_Guard is true
 * AJ1_AA_Guard is false
 * AJ1_AB_Guard is true
 * All other guards are false
 *
 * When given a start transition of Initial -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_to_AB_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J1_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    BTCSH_J1_B_Guard_IgnoreAndReturn(false);
    BTCSH_J1_C_Guard_IgnoreAndReturn(false);
    BTCSH_AJ1_AC_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_Initial_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Initial to J1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_J1_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* J1 to A */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_InitialA_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* InitialA to AJ1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_AJ1_Outbound_Transitions[1], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* AJ1 to AB */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * Initial -> J1
 * J1 -> B
 * InitialB -> BJ1
 * BJ1 -> BB
 *
 * And the following Guards:
 *
 * J1_A_Guard is false
 * J1_B_Guard is true
 * BJ1_BA_Guard is false
 * BJ1_BB_Guard is true
 * All other guards are false
 *
 * When given a start transition of Initial -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_to_BB_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J1_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J1_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_BJ1_BA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    BTCSH_J1_C_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_Initial_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Initial to J1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_J1_Outbound_Transitions[1], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* J1 to B */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_InitialB_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* InitialB to BJ1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_BJ1_Outbound_Transitions[1], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* BJ1 to BB */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * Initial -> J1
 * J1 -> C
 * InitialC -> CJ1
 * CJ1 -> CC
 *
 * And the following Guards:
 *
 * J1_A_Guard is true
 * AJ1_AA_Guard is false
 * AJ1_AB_Guard is false
 * AJ1_AC_Guard is false
 * J1_B_Guard is true
 * BJ1_BA_Guard is false
 * BJ1_BB_Guard is false
 * BJ1_BC_Guard is false
 * J1_C_Guard is true
 * CJ1_CA_Guard is false
 * CJ1_CB_Guard is false
 * CJ1_CC_Guard is true
 * When given a start transition of Initial -> J1
 *
 * This test the scenario when all first level guards are true but second level guards are false.
 * In other words a valid path to A and B is found but no simple state can be reached within those
 * compound states
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_to_CC_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J1_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J1_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_BJ1_BA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J1_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_CJ1_CA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_CJ1_CB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_CJ1_CC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_Initial_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* Initial to J1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_J1_Outbound_Transitions[2], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* J1 to C */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_InitialC_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* InitialC to CJ1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_CJ1_Outbound_Transitions[2], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* CJ1 to CC */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition chain when no valid path is found
 * to a simple state. All first level guards are false.
 *
 * And the following Guards:
 *
 * J1_A_Guard is false
 * J1_B_Guard is false
 * J1_C_Guard is false
 * When given a start transition of Initial -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_No_Valid_Path_1st_level_Guards_false_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J1_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J1_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J1_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition chain when no valid path is found
 * to a simple state. All first level guards are true, second level guards are false
 *
 * And the following Guards:
 *
 * J1_A_Guard is true
 * AJ1_AA_Guard is false
 * AJ1_AB_Guard is false
 * AJ1_AC_Guard is false
 * J1_B_Guard is true
 * BJ1_BA_Guard is false
 * BJ1_BB_Guard is false
 * BJ1_BC_Guard is false
 * J1_C_Guard is true
 * CJ1_CA_Guard is false
 * CJ1_CB_Guard is false
 * CJ1_CC_Guard is false
 * When given a start transition of Initial -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_Initial_To_J1_Transition_As_Start_No_Valid_Path_2nd_level_Guards_false_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J1_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J1_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_BJ1_BA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J1_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_CJ1_CA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_CJ1_CB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_CJ1_CC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> J2
 * J2 -> A
 * InitialA -> AJ1
 * AJ1 -> AA
 *
 * And the following Guards:
 *
 * J2_A_Guard is true
 * AJ1_AA_Guard is true
 * All other guards are false
 *
 * When given a start transition of A -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_A_To_J2_Transition_As_Start_to_AA_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    BTCSH_J2_B_Guard_IgnoreAndReturn(false);
    BTCSH_J2_C_Guard_IgnoreAndReturn(false);
    BTCSH_AJ1_AB_Guard_IgnoreAndReturn(false);
    BTCSH_AJ1_AC_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_A_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* A to J2 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_J2_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* J2 to A */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_InitialA_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* InitialA to AJ1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_AJ1_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* AJ1 to AA */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> J2
 * J2 -> A
 * InitialA -> AJ1
 * AJ1 -> AB
 *
 * And the following Guards:
 *
 * J1_A_Guard is true
 * AJ1_AA_Guard is false
 * AJ1_AB_Guard is true
 * All other guards are false
 *
 * When given a start transition of Initial -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_A_To_J2_Transition_As_Start_to_AB_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    BTCSH_J2_B_Guard_IgnoreAndReturn(false);
    BTCSH_J2_C_Guard_IgnoreAndReturn(false);
    BTCSH_AJ1_AC_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_A_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* A to J2 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_J2_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* J2 to A */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_InitialA_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* InitialA to AJ1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_AJ1_Outbound_Transitions[1], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* AJ1 to AB */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> J2
 * J2 -> B
 * InitialB -> BJ1
 * BJ1 -> BB
 *
 * And the following Guards:
 *
 * J2_A_Guard is false
 * J2_B_Guard is true
 * BJ1_BA_Guard is false
 * BJ1_BB_Guard is true
 * All other guards are false
 *
 * When given a start transition of Initial -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_A_To_J2_Transition_As_Start_to_BB_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J2_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_BJ1_BA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    BTCSH_J2_C_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_A_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* A to J2 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_J2_Outbound_Transitions[1], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* J2 to B */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_InitialB_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* InitialB to BJ1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_BJ1_Outbound_Transitions[1], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* BJ1 to BB */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create a transition chain with the following transitions:
 *
 * A -> J2
 * J2 -> C
 * InitialC -> CJ1
 * CJ1 -> CC
 *
 * And the following Guards:
 *
 * J2_A_Guard is true
 * AJ1_AA_Guard is false
 * AJ1_AB_Guard is false
 * AJ1_AC_Guard is false
 * J2_B_Guard is true
 * BJ1_BA_Guard is false
 * BJ1_BB_Guard is false
 * BJ1_BC_Guard is false
 * J2_C_Guard is true
 * CJ1_CA_Guard is false
 * CJ1_CB_Guard is false
 * CJ1_CC_Guard is true
 * When given a start transition of A -> J2
 *
 * This test the scenario when all first level guards are true but second level guards are false.
 * In other words a valid path to A and B is found but no simple state can be reached within those
 * compound states
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_A_To_J2_Transition_As_Start_to_CC_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J2_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_BJ1_BA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_CJ1_CA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_CJ1_CB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_CJ1_CC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(4, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_SUCCESS, result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_A_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[0].Transition); /* A to J2 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_J2_Outbound_Transitions[2], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[1].Transition); /* J2 to C */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_InitialC_Outbound_Transitions[0], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[2].Transition); /* InitialC to CJ1 */
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_CJ1_Outbound_Transitions[2], Branching_Composite_HSM.Transition_Chain_Data.Transition_Chain[3].Transition); /* CJ1 to CC */
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition chain when no valid path is found
 * to a simple state. All first level guards are false.
 *
 * And the following Guards:
 *
 * J2_A_Guard is false
 * J2_B_Guard is false
 * J2_C_Guard is false
 * When given a start transition of A -> J2
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_A_To_J2_Transition_As_Start_No_Valid_Path_1st_level_Guards_false_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J2_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_VALID_CHAIN_NOT_FOUND, result);
}

/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition chain when no valid path is found
 * to a simple state. All first level guards are true, second level guards are false
 *
 * And the following Guards:
 *
 * J2_A_Guard is true
 * AJ1_AA_Guard is false
 * AJ1_AB_Guard is false
 * AJ1_AC_Guard is false
 * J2_B_Guard is true
 * BJ1_BA_Guard is false
 * BJ1_BB_Guard is false
 * BJ1_BC_Guard is false
 * J2_C_Guard is true
 * CJ1_CA_Guard is false
 * CJ1_CB_Guard is false
 * CJ1_CC_Guard is false
 * When given a start transition of A -> J2
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_A_To_J2_Transition_As_Start_No_Valid_Path_2nd_level_Guards_false_BTCSH(void)
{
    HSM_Transition_T * Start_Transition = &BTCSH_State_A_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */
    BTCSH_J2_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_AJ1_AC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J2_B_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_BJ1_BA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_BJ1_BC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_J2_C_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_CJ1_CA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_CJ1_CB_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);
    BTCSH_CJ1_CC_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, false);

    /* Call function under test */
    result = hsm_Build_Transition(&Branching_Composite_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Branching_Composite_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_VALID_CHAIN_NOT_FOUND, result);
}

/*-------------------------------------*\
 * Partial statechart used for testing *
 * a chain that is too long.           *
\*-------------------------------------*/
HSM_State_T const Too_Long_State_A;
HSM_State_T const Too_Long_State_Initial;
HSM_State_T const Too_Long_State_J1;
HSM_State_T const Too_Long_State_J2;
HSM_State_T const Too_Long_State_J3;
HSM_State_T const Too_Long_State_J4;
HSM_State_T const Too_Long_State_J5;
HSM_State_T const Too_Long_State_J6;

HSM_Transition_T const Too_Long_State_Initial_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &Too_Long_State_Initial,
        .Target = &Too_Long_State_J1,
    },
};

HSM_Transition_T const Too_Long_State_J1_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &Too_Long_State_J1,
        .Target = &Too_Long_State_J2,
    },
};
HSM_Transition_T const Too_Long_State_J2_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &Too_Long_State_J2,
        .Target = &Too_Long_State_J3,
    },
};
HSM_Transition_T const Too_Long_State_J3_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &Too_Long_State_J3,
        .Target = &Too_Long_State_J4,
    },
};
HSM_Transition_T const Too_Long_State_J4_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &Too_Long_State_J4,
        .Target = &Too_Long_State_J5,
    },
};
HSM_Transition_T const Too_Long_State_J5_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &Too_Long_State_J5,
        .Target = &Too_Long_State_J6,
    },
};
HSM_Transition_T const Too_Long_State_J6_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &Too_Long_State_J6,
        .Target = &Too_Long_State_A,
    },
};

HSM_State_T const Too_Long_State_A =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "A",
};

HSM_State_T const Too_Long_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CTH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const Too_Long_State_J1 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(Too_Long_State_J1_Outbound_Transitions),
    .Outbound_Transitions = Too_Long_State_J1_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const Too_Long_State_J2 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(Too_Long_State_J2_Outbound_Transitions),
    .Outbound_Transitions = Too_Long_State_J2_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J2",
};

HSM_State_T const Too_Long_State_J3 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(Too_Long_State_J3_Outbound_Transitions),
    .Outbound_Transitions = Too_Long_State_J3_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J3",
};

HSM_State_T const Too_Long_State_J4 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(Too_Long_State_J4_Outbound_Transitions),
    .Outbound_Transitions = Too_Long_State_J4_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J4",
};

HSM_State_T const Too_Long_State_J5 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(Too_Long_State_J5_Outbound_Transitions),
    .Outbound_Transitions = Too_Long_State_J5_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J5",
};

HSM_State_T const Too_Long_State_J6 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(Too_Long_State_J6_Outbound_Transitions),
    .Outbound_Transitions = Too_Long_State_J6_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J6",
};

HSM_T Too_Long_HSM =
{
    .Current_State = NULL,
    .Initial_State = &Too_Long_State_Initial,
};

/*-------------------------------------*\
 * End                                 *
 * Partial statechart used for testing *
 * a chain that is too long.           *
\*-------------------------------------*/
/**
 * @test Verifies that the function hsm_Build_Transition()
 * will create an empty transition chain when the valid path
 * contains too many transitions.
 *
 * When given a start transition of Initial -> J1
 *
 * Statemachine: Compound Transition/Composite State Statemachine
 */
void test_hsm_Build_Transition_Initial_To_A_Transition_As_Start_Path_Too_Long(void)
{
    HSM_Transition_T * Start_Transition = &Too_Long_State_Initial_Outbound_Transitions[0];
    HSM_Status_T result = HSM_NUM_STATUS;

    /* Setup expected call chain */

    /* Call function under test */
    result = hsm_Build_Transition(&Too_Long_HSM, Start_Transition, HSM_NO_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL(0, Too_Long_HSM.Transition_Chain_Data.Chain_Size);
    TEST_ASSERT_EQUAL(HSM_CHAIN_TOO_LONG, result);
}
