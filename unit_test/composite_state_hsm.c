/**
 *  @file composite_state_hsm.c
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "composite_state_hsm.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*------------------------------------------------*\
 * Begin: Composite State Statemachine definition *
\*------------------------------------------------*/
HSM_Transition_T const CSH_State_A_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_A_B_Guard,
        .Effect = CSH_A_B_Effect,
        .Source = &CSH_State_A,
        .Target = &CSH_State_B,
    },
};

HSM_Transition_T const CSH_State_AA_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_AA_AB_Guard,
        .Effect = CSH_AA_AB_Effect,
        .Source = &CSH_State_AA,
        .Target = &CSH_State_AB,
    },
};

HSM_Transition_T const CSH_State_AAA_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_AAA_AAB_Guard,
        .Effect = CSH_AAA_AAB_Effect,
        .Source = &CSH_State_AAA,
        .Target = &CSH_State_AAB,
    },
};

HSM_Transition_T const CSH_State_AAB_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_AAB_AAA_Guard,
        .Effect = CSH_AAB_AAA_Effect,
        .Source = &CSH_State_AAB,
        .Target = &CSH_State_AAA,
    },
};

HSM_Transition_T const CSH_State_AB_Outbound_Transitions[2] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_AB_AA_Guard,
        .Effect = CSH_AB_AA_Effect,
        .Source = &CSH_State_AB,
        .Target = &CSH_State_AA,
    },
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_AB_AC_Guard,
        .Effect = CSH_AB_AC_Effect,
        .Source = &CSH_State_AB,
        .Target = &CSH_State_AC,
    },
};

HSM_Transition_T const CSH_State_ABA_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_ABA_ABB_Guard,
        .Effect = CSH_ABA_ABB_Effect,
        .Source = &CSH_State_ABA,
        .Target = &CSH_State_ABB,
    },
};

HSM_Transition_T const CSH_State_ABB_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_ABB_ABA_Guard,
        .Effect = CSH_ABB_ABA_Effect,
        .Source = &CSH_State_ABB,
        .Target = &CSH_State_ABA,
    },
};

HSM_Transition_T const CSH_State_AC_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_AC_AA_Guard,
        .Effect = CSH_AC_AA_Effect,
        .Source = &CSH_State_AC,
        .Target = &CSH_State_AA,
    },
};

HSM_Transition_T const CSH_State_B_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_B_A_Guard,
        .Effect = CSH_B_A_Effect,
        .Source = &CSH_State_B,
        .Target = &CSH_State_A,
    },
};

HSM_Transition_T const CSH_State_BA_Outbound_Transitions[2] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_BA_BB_Guard,
        .Effect = CSH_BA_BB_Effect,
        .Source = &CSH_State_BA,
        .Target = &CSH_State_BB,
    },
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_BA_BC_Guard,
        .Effect = CSH_BA_BC_Effect,
        .Source = &CSH_State_BA,
        .Target = &CSH_State_BC,
    },
};

HSM_Transition_T const CSH_State_BAA_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_BAA_BAB_Guard,
        .Effect = CSH_BAA_BAB_Effect,
        .Source = &CSH_State_BAA,
        .Target = &CSH_State_BAB,
    },
};

HSM_Transition_T const CSH_State_BAB_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_BAB_BAA_Guard,
        .Effect = CSH_BAB_BAA_Effect,
        .Source = &CSH_State_BAB,
        .Target = &CSH_State_BAA,
    },
};

HSM_Transition_T const CSH_State_BB_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_BB_BA_Guard,
        .Effect = CSH_BB_BA_Effect,
        .Source = &CSH_State_BB,
        .Target = &CSH_State_BA,
    },
};

HSM_Transition_T const CSH_State_BBA_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_BBA_BBB_Guard,
        .Effect = CSH_BBA_BBB_Effect,
        .Source = &CSH_State_BBA,
        .Target = &CSH_State_BBB,
    },
};

HSM_Transition_T const CSH_State_BBB_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_BBB_BBA_Guard,
        .Effect = CSH_BBB_BBA_Effect,
        .Source = &CSH_State_BBB,
        .Target = &CSH_State_BBA,
    },
};

HSM_Transition_T const CSH_State_BC_Outbound_Transitions[1] =
{
    {
        .Trigger = CSH_TRIGGER_1,
        .Guard = CSH_BC_BB_Guard,
        .Effect = CSH_BC_BB_Effect,
        .Source = &CSH_State_BC,
        .Target = &CSH_State_BB,
    },
};

HSM_Transition_T const CSH_State_Initial_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CSH_Initial_Effect,
        .Source = &CSH_State_Initial,
        .Target = &CSH_State_A,
    },
};

HSM_Transition_T const CSH_State_InitialA_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CSH_InitialA_Effect,
        .Source = &CSH_State_InitialA,
        .Target = &CSH_State_AA,
    },
};

HSM_Transition_T const CSH_State_InitialAA_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CSH_InitialAA_Effect,
        .Source = &CSH_State_InitialAA,
        .Target = &CSH_State_AAA,
    },
};

HSM_Transition_T const CSH_State_InitialAB_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CSH_InitialAB_Effect,
        .Source = &CSH_State_InitialAB,
        .Target = &CSH_State_ABA,
    },
};

HSM_Transition_T const CSH_State_InitialB_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CSH_InitialB_Effect,
        .Source = &CSH_State_InitialB,
        .Target = &CSH_State_BA,
    },
};

HSM_Transition_T const CSH_State_InitialBA_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CSH_InitialBA_Effect,
        .Source = &CSH_State_InitialBA,
        .Target = &CSH_State_BAA,
    },
};

HSM_Transition_T const CSH_State_InitialBB_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = CSH_InitialBB_Effect,
        .Source = &CSH_State_InitialBB,
        .Target = &CSH_State_BBA,
    },
};

HSM_State_T const CSH_State_A =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &CSH_State_InitialA,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_A_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_A_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_A_Entry,
    .Exit_Behavior = CSH_A_Exit,
    .Name = "A",
};

HSM_State_T const CSH_State_AA =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &CSH_State_InitialAA,
    .Parent_State = &CSH_State_A,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_AA_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_AA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_AA_Entry,
    .Exit_Behavior = CSH_AA_Exit,
    .Name = "AA",
};

HSM_State_T const CSH_State_AAA =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_AA,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_AAA_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_AAA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_AAA_Entry,
    .Exit_Behavior = CSH_AAA_Exit,
    .Name = "AAA",
};

HSM_State_T const CSH_State_AAB =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_AA,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_AAB_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_AAB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_AAB_Entry,
    .Exit_Behavior = CSH_AAB_Exit,
    .Name = "AAB",
};

HSM_State_T const CSH_State_AB =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &CSH_State_InitialAB,
    .Parent_State = &CSH_State_A,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_AB_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_AB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_AB_Entry,
    .Exit_Behavior = CSH_AB_Exit,
    .Name = "AB",
};

HSM_State_T const CSH_State_ABA =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_AB,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_ABA_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_ABA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_ABA_Entry,
    .Exit_Behavior = CSH_ABA_Exit,
    .Name = "ABA",
};

HSM_State_T const CSH_State_ABB =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_AB,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_ABB_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_ABB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_ABB_Entry,
    .Exit_Behavior = CSH_ABB_Exit,
    .Name = "ABB",
};

HSM_State_T const CSH_State_AC =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_A,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_AC_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_AC_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_AC_Entry,
    .Exit_Behavior = CSH_AC_Exit,
    .Name = "AC",
};

HSM_State_T const CSH_State_B =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &CSH_State_InitialB,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_B_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_B_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_B_Entry,
    .Exit_Behavior = CSH_B_Exit,
    .Name = "B",
};

HSM_State_T const CSH_State_BA =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &CSH_State_InitialBA,
    .Parent_State = &CSH_State_B,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_BA_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_BA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_BA_Entry,
    .Exit_Behavior = CSH_BA_Exit,
    .Name = "BA",
};

HSM_State_T const CSH_State_BAA =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_BA,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_BAA_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_BAA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_BAA_Entry,
    .Exit_Behavior = CSH_BAA_Exit,
    .Name = "BAA",
};

HSM_State_T const CSH_State_BAB =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_BA,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_BAB_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_BAB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_BAB_Entry,
    .Exit_Behavior = CSH_BAB_Exit,
    .Name = "BAB",
};

HSM_State_T const CSH_State_BB =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = &CSH_State_InitialBB,
    .Parent_State = &CSH_State_B,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_BB_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_BB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_BB_Entry,
    .Exit_Behavior = CSH_BB_Exit,
    .Name = "BB",
};

HSM_State_T const CSH_State_BBA =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_BB,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_BBA_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_BBA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_BBA_Entry,
    .Exit_Behavior = CSH_BBA_Exit,
    .Name = "BBA",
};

HSM_State_T const CSH_State_BBB =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_BB,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_BBB_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_BBB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_BBB_Entry,
    .Exit_Behavior = CSH_BBB_Exit,
    .Name = "BBB",
};

HSM_State_T const CSH_State_BC =
{
    .Kind = HSM_COMPOSITE,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_B,
    .Num_Outbound_Transitions = Num_Elems(CSH_State_BC_Outbound_Transitions),
    .Outbound_Transitions = CSH_State_BC_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = CSH_BC_Entry,
    .Exit_Behavior = CSH_BC_Exit,
    .Name = "BC",
};

HSM_State_T const CSH_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CSH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const CSH_State_InitialA =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_A,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CSH_State_InitialA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "InitialA",
};

HSM_State_T const CSH_State_InitialAA =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_AA,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CSH_State_InitialAA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "InitialAA",
};

HSM_State_T const CSH_State_InitialAB =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_AB,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CSH_State_InitialAB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "InitialAB",
};

HSM_State_T const CSH_State_InitialB =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_B,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CSH_State_InitialB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "InitialB",
};

HSM_State_T const CSH_State_InitialBA =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_BA,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CSH_State_InitialBA_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "InitialBA",
};

HSM_State_T const CSH_State_InitialBB =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = &CSH_State_BB,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = CSH_State_InitialBB_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "InitialBB",
};

HSM_T Composite_State_HSM =
{
    .Current_State = NULL,
    .Initial_State = &CSH_State_Initial,
};

/*----------------------------------------------*\
 * End: Composite State Statemachine definition *
\*----------------------------------------------*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
