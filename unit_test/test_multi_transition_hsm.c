/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "hsm_engine.h"
#include "multi_transition_hsm.h"

/* MOCKS */
#include "mock_multi_transition_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_basic_test_hsm.h"
#include "mock_branching_transition_hsm.h"
#include "mock_branching_transition_composite_state_hsm.h"
#include "mock_composite_state_hsm.h"
#include "mock_compound_transition_hsm.h"
#include "mock_else_guards_hsm.h"
#include "mock_internal_transitions_hsm.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the Statemachine engine will take the only transition
 * out of the initial state and transition to the target state, executing the
 * the Initial Effect and the Target State's entry.
 */
void test_HSM_Start_Enables_initial_transition_and_Enters_A(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = NULL;

    /* Setup expected call chain */
    MTH_A_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Multi_Transition_HSM);
    
    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&MTH_State_A, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_A_TRIGGER_A_B_Guard_true_in_A_traverses_A_B_Transition(void)
{
    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_A;

    /* Setup expected call chain */
    MTH_A_B_Guard_ExpectAndReturn(MTH_LEAVE_A_TRIGGER, 0, true);
    MTH_A_Exit_Expect();
    MTH_A_B_Effect_Expect();
    MTH_B_Entry_Expect();

    MTH_A_C_Guard_IgnoreAndReturn(false);

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_LEAVE_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_B, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_A_TRIGGER_A_B_Guard_false_A_C_Guard_true_in_A_traverses_A_C_Transition(void)
{

    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_A;

    /* Setup expected call chain */
    MTH_A_B_Guard_ExpectAndReturn(MTH_LEAVE_A_TRIGGER, 0, false);
    MTH_A_C_Guard_ExpectAndReturn(MTH_LEAVE_A_TRIGGER, 0, true);
    MTH_A_Exit_Expect();
    MTH_A_C_Effect_Expect();
    MTH_C_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_LEAVE_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_C, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_A_TRIGGER_A_B_Guard_false_A_C_Guard_false_in_A_stays_in_A(void)
{
    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_A;

    /* Setup expected call chain */
    MTH_A_B_Guard_ExpectAndReturn(MTH_LEAVE_A_TRIGGER, 0, false);
    MTH_A_C_Guard_ExpectAndReturn(MTH_LEAVE_A_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_LEAVE_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_A, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_C_TRIGGER_in_A_dropped(void)
{
    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_A;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_LEAVE_C_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_A, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_B_TRIGGER_in_A_dropped(void)
{
    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_A;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_LEAVE_B_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_A, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_A_B_NE_TRIGGER_Guard_true_in_A_traverses_A_B_Transition(void)
{
    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_A;

    /* Setup expected call chain */
    MTH_A_B_Guard_ExpectAndReturn(MTH_A_B_NE_TRIGGER, 0, true);
    MTH_A_Exit_Expect();
    MTH_B_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_A_B_NE_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_B, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_A_B_NG_TRIGGER_in_A_traverses_A_B_Transition(void)
{
    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_A;

    /* Setup expected call chain */
    MTH_A_Exit_Expect();
    MTH_A_B_Effect_Expect();
    MTH_B_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_A_B_NG_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_B, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_A_B_NG_NE_TRIGGER_in_A_traverses_A_B_Transition(void)
{
    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_A;

    /* Setup expected call chain */
    MTH_A_Exit_Expect();
    MTH_B_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_A_B_NG_NE_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_B, Multi_Transition_HSM.Current_State);
}

void test_HSM_Process_Event_LEAVE_C_TRIGGER_C_A_Guard_false_C_B_Guard_false_C_C_Guard_true_in_C_traverses_C_C_Transition(void)
{

    /* Ensure known test state */
    Multi_Transition_HSM.Current_State = &MTH_State_C;

    /* Setup expected call chain */
    MTH_C_A_Guard_ExpectAndReturn(MTH_LEAVE_C_TRIGGER, 0, false);
    MTH_C_B_Guard_ExpectAndReturn(MTH_LEAVE_C_TRIGGER, 0, false);
    MTH_C_C_Guard_ExpectAndReturn(MTH_LEAVE_C_TRIGGER, 0, true);
    MTH_C_Exit_Expect();
    MTH_C_C_Effect_Expect();
    MTH_C_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Multi_Transition_HSM, MTH_LEAVE_C_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&MTH_State_C, Multi_Transition_HSM.Current_State);
}
