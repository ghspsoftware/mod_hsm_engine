/**
 *  @file basic_test_hsm.c
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "basic_test_hsm.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*-------------------------------------------*\
 * Begin: Basic Test Statemachine definition *
\*-------------------------------------------*/

HSM_Transition_T const BTH_State_A_Outbound_Transitions[1] =
{
    {
        .Trigger = BTH_A_B_TRIGGER,
        .Guard = BTH_A_B_Guard,
        .Effect = BTH_A_B_Effect,
        .Source = &BTH_State_A,
        .Target = &BTH_State_B,
    },
};

HSM_Transition_T const BTH_State_B_Outbound_Transitions[1] =
{
    {
        .Trigger = BTH_B_C_TRIGGER,
        .Guard = BTH_B_C_Guard,
        .Effect = BTH_B_C_Effect,
        .Source = &BTH_State_B,
        .Target = &BTH_State_C,
    },
};

HSM_Transition_T const BTH_State_C_Outbound_Transitions[1] =
{
    {
        .Trigger = BTH_C_A_TRIGGER,
        .Guard = BTH_C_A_Guard,
        .Effect = BTH_C_A_Effect,
        .Source = &BTH_State_C,
        .Target = &BTH_State_A,
    },
};

HSM_Transition_T const BTH_State_Initial_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = BTH_Initial_Effect,
        .Source = &BTH_State_Initial,
        .Target = &BTH_State_A,
    },
};

HSM_State_T const BTH_State_A =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTH_State_A_Outbound_Transitions),
    .Outbound_Transitions = BTH_State_A_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTH_A_Entry,
    .Exit_Behavior = BTH_A_Exit,
    .Name = "A",
};

HSM_State_T const BTH_State_B =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTH_State_B_Outbound_Transitions),
    .Outbound_Transitions = BTH_State_B_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTH_B_Entry,
    .Exit_Behavior = BTH_B_Exit,
    .Name = "B",
};

HSM_State_T const BTH_State_C =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BTH_State_C_Outbound_Transitions),
    .Outbound_Transitions = BTH_State_C_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BTH_C_Entry,
    .Exit_Behavior = BTH_C_Exit,
    .Name = "C",
};

HSM_State_T const BTH_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = BTH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_T Basic_Test_HSM =
{
    .Current_State = NULL,
    .Initial_State = &BTH_State_Initial,
};

/*-----------------------------------------*\
 * End: Basic Test Statemachine definition *
\*-----------------------------------------*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
