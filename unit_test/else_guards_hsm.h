#ifndef ELSE_GUARD_HSM_H
#define ELSE_GUARD_HSM_H
/**
 *  @file basic_test_hsm.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup basic_test_hsm_imp
 *  @{
 *
 *      API that exposes the prototypes of all Guard and Behavior functions
 *      of the Basic Test HSM and data structures used for mocking and testing.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    EGH_EXIT_A_TRIGGER
} Else_Guard_HSM_Trigger_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
HSM_State_T const EGH_State_A;
HSM_State_T const EGH_State_B;
HSM_State_T const EGH_State_C;
HSM_State_T const EGH_State_D;
HSM_State_T const EGH_State_E;
HSM_State_T const EGH_State_F;
HSM_State_T const EGH_State_G;
HSM_State_T const EGH_State_Initial;
HSM_State_T const EGH_State_J1;
HSM_State_T const EGH_State_J2;
HSM_State_T const EGH_State_J3;
HSM_State_T const EGH_State_J4;
HSM_State_T const EGH_State_J6;
HSM_State_T const EGH_State_J7;
HSM_State_T const EGH_State_J8;

HSM_Transition_T const EGH_State_A_Outbound_Transitions[3];
HSM_Transition_T const EGH_State_J1_Outbound_Transitions[3];
HSM_Transition_T const EGH_State_J2_Outbound_Transitions[3];
HSM_Transition_T const EGH_State_J3_Outbound_Transitions[3];
HSM_Transition_T const EGH_State_J4_Outbound_Transitions[3];
HSM_Transition_T const EGH_State_J6_Outbound_Transitions[3];
HSM_Transition_T const EGH_State_J7_Outbound_Transitions[3];
HSM_Transition_T const EGH_State_J8_Outbound_Transitions[3];
HSM_Transition_T const EGH_State_Initial_Outbound_Transitions[1];

HSM_T Else_Guard_HSM;

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/*-----------------------*\
 * State Entry functions *
\*-----------------------*/

/*-----------------------*\
 * State Exit functions *
\*-----------------------*/

/*----------------------------*\
 * Transition Guard functions *
\*----------------------------*/
bool_t EGH_A_J6_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_A_J7_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J1_J2_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J1_J3_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J2_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J2_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J3_D_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J3_E_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J4_E_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J4_G_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J6_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J6_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J7_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J7_E_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J8_F_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t EGH_J8_G_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/*-----------------------------*\
 * Transition Effect functions *
\*-----------------------------*/

/** @} doxygen end group */
#endif /* ELSE_GUARD_HSM_H */
