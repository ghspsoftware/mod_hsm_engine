#ifndef MULTI_TRANSITION_HSM_H
#define MULTI_TRANSITION_HSM_H
/**
 *  @file multi_transition_hsm.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup multi_transition_hsm_imp
 *  @{
 *
 *      API that exposes the prototypes of all Guard and Behavior functions
 *      of the Multi Transition HSM and the data structures used for mocking
 *      and testing.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    MTH_LEAVE_A_TRIGGER,
    MTH_LEAVE_B_TRIGGER,
    MTH_LEAVE_C_TRIGGER,
    MTH_A_B_NE_TRIGGER,
    MTH_A_B_NG_TRIGGER,
    MTH_A_B_NG_NE_TRIGGER
} Multi_Transition_HSM_Trigger_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
HSM_State_T const MTH_State_A;
HSM_State_T const MTH_State_B;
HSM_State_T const MTH_State_C;
HSM_State_T const MTH_State_Initial;

HSM_Transition_T const MTH_State_A_Outbound_Transitions[5];
HSM_Transition_T const MTH_State_B_Outbound_Transitions[2];
HSM_Transition_T const MTH_State_C_Outbound_Transitions[3];
HSM_Transition_T const MTH_State_Initial_Outbound_Transitions[1];

HSM_T Multi_Transition_HSM;

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/*-----------------------*\
 * State Entry functions *
\*-----------------------*/
void MTH_A_Entry(void);
void MTH_B_Entry(void);
void MTH_C_Entry(void);

/*-----------------------*\
 * State Exit functions *
\*-----------------------*/
void MTH_A_Exit(void);
void MTH_B_Exit(void);
void MTH_C_Exit(void);

/*----------------------------*\
 * Transition Guard functions *
\*----------------------------*/
bool_t MTH_A_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t MTH_A_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t MTH_B_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t MTH_B_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t MTH_C_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t MTH_C_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t MTH_C_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/*-----------------------------*\
 * Transition Effect functions *
\*-----------------------------*/
void MTH_A_B_Effect(void);
void MTH_A_C_Effect(void);
void MTH_B_C_Effect(void);
void MTH_B_A_Effect(void);
void MTH_C_A_Effect(void);
void MTH_C_B_Effect(void);
void MTH_C_C_Effect(void);

/** @} doxygen end group */
#endif /* MULTI_TRANSITION_HSM_H */
