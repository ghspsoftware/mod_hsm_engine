#ifndef BRANCHING_TRANSITION_HSM_H
#define BRANCHING_TRANSITION_HSM_H
/**
 *  @file branching_transition_hsm.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup basic_test_hsm_imp
 *  @{
 *
 *      API that exposes the prototypes of all Guard and Behavior functions
 *      of the Branching Transition HSM and data structures used for mocking
 *      and testing.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    BRTH_A_J5_TRIGGER,
    BRTH_A_B_TRIGGER,
    BRTH_D_J5_TRIGGER
} Branching_Transition_HSM_Trigger_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
HSM_State_T const BRTH_State_A;
HSM_State_T const BRTH_State_B;
HSM_State_T const BRTH_State_C;
HSM_State_T const BRTH_State_D;
HSM_State_T const BRTH_State_Initial;
HSM_State_T const BRTH_State_J1;
HSM_State_T const BRTH_State_J2;
HSM_State_T const BRTH_State_J3;
HSM_State_T const BRTH_State_J4;
HSM_State_T const BRTH_State_J5;
HSM_State_T const BRTH_State_J6;
HSM_State_T const BRTH_State_J7;
HSM_State_T const BRTH_State_J8;
HSM_State_T const BRTH_State_J9;
HSM_State_T const BRTH_State_J10;

HSM_Transition_T const BRTH_State_A_Outbound_Transitions[2];
HSM_Transition_T const BRTH_State_D_Outbound_Transitions[1];
HSM_Transition_T const BRTH_State_Initial_Outbound_Transitions[1];
HSM_Transition_T const BRTH_State_J1_Outbound_Transitions[3];
HSM_Transition_T const BRTH_State_J2_Outbound_Transitions[3];
HSM_Transition_T const BRTH_State_J3_Outbound_Transitions[1];
HSM_Transition_T const BRTH_State_J4_Outbound_Transitions[2];
HSM_Transition_T const BRTH_State_J5_Outbound_Transitions[4];
HSM_Transition_T const BRTH_State_J6_Outbound_Transitions[4];
HSM_Transition_T const BRTH_State_J7_Outbound_Transitions[1];
HSM_Transition_T const BRTH_State_J8_Outbound_Transitions[1];
HSM_Transition_T const BRTH_State_J9_Outbound_Transitions[1];
HSM_Transition_T const BRTH_State_J10_Outbound_Transitions[1];

HSM_T Branching_Transition_HSM;

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/*-----------------------*\
 * State Entry functions *
\*-----------------------*/
void BRTH_A_Entry(void);
void BRTH_B_Entry(void);
void BRTH_C_Entry(void);
void BRTH_D_Entry(void);

/*-----------------------*\
 * State Exit functions *
\*-----------------------*/
void BRTH_A_Exit(void);
void BRTH_B_Exit(void);
void BRTH_C_Exit(void);
void BRTH_D_Exit(void);

/*----------------------------*\
 * Transition Guard functions *
\*----------------------------*/
bool_t BRTH_A_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_A_J5_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_D_J5_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J1_J2_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J1_J3_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J1_J4_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J2_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J2_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J2_J8_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J3_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J4_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J4_J7_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J5_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J5_J6_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J5_J9_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J5_J10_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J6_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J6_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J6_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J6_D_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BRTH_J10_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/*-----------------------------*\
 * Transition Effect functions *
\*-----------------------------*/
void BRTH_A_B_Effect(void);
void BRTH_A_J5_Effect(void);
void BRTH_D_J5_Effect(void);
void BRTH_Initial_Effect(void);
void BRTH_J1_J2_Effect(void);
void BRTH_J1_J3_Effect(void);
void BRTH_J1_J4_Effect(void);
void BRTH_J2_A_Effect(void);
void BRTH_J2_C_Effect(void);
void BRTH_J3_B_Effect(void);
void BRTH_J4_B_Effect(void);
void BRTH_J4_J7_Effect(void);
void BRTH_J5_A_Effect(void);
void BRTH_J5_J6_Effect(void);
void BRTH_J5_J10_Effect(void);
void BRTH_J6_A_Effect(void);
void BRTH_J6_B_Effect(void);
void BRTH_J6_C_Effect(void);
void BRTH_J7_C_Effect(void);
void BRTH_J9_C_Effect(void);
void BRTH_J10_C_Effect(void);

/** @} doxygen end group */
#endif /* BRANCHING_TRANSITION_HSM_H */
