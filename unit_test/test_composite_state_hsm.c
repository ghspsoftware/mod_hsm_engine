/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "composite_state_hsm.h"
#include "hsm_engine.h"

/* MOCKS */
#include "mock_composite_state_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_basic_test_hsm.h"
#include "mock_branching_transition_hsm.h"
#include "mock_branching_transition_composite_state_hsm.h"
#include "mock_compound_transition_hsm.h"
#include "mock_else_guards_hsm.h"
#include "mock_internal_transitions_hsm.h"
#include "mock_multi_transition_hsm.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the Statemachine engine will take the only transition
 * out of the initial state and transition to A. The initial state of A will then
 * take it's only outbound transition to AA. The initial state of AA will then
 * take it's only outbound transition to AAA. As each of those states is entered
 * the Entry function is executed. Execution order is as follows:
 *
 * Initial_Effect
 * A_Entry
 * InitialA_Effect
 * AA_Entry
 * InitialAA_Effect
 * AAA_Entry
 */
void test_HSM_Start_Enters_AAA_executing_all_Initial_and_Entry_effects(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Composite_State_HSM.Current_State = NULL;

    /* Setup expected call chain */
    CSH_Initial_Effect_Expect();
    CSH_A_Entry_Expect();
    CSH_InitialA_Effect_Expect();
    CSH_AA_Entry_Expect();
    CSH_InitialAA_Effect_Expect();
    CSH_AAA_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Composite_State_HSM);
    
    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&CSH_State_AAA, Composite_State_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will take the transition from AAA
 * to AAB when it receives TRIGGER_1 and AAA_AAB_Guard is true. This will execute
 * AAA_Exit, AAA_AAB_Effect, then AAB_Entry
 */
void test_HSM_Process_Event_TRIGGER_1_AAA_AAB_G_true_In_AAA_goes_to_AAB(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Composite_State_HSM.Current_State = &CSH_State_AAA;

    /* Setup expected call chain */
    CSH_AAA_AAB_Guard_ExpectAndReturn(CSH_TRIGGER_1, 0, true);

    CSH_AAA_Exit_Expect();
    CSH_AAA_AAB_Effect_Expect();
    CSH_AAB_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Composite_State_HSM, CSH_TRIGGER_1, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_AAB, Composite_State_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will take the transition from AAA
 * to ABA when it receives TRIGGER_1, AAA_AAB_Guard is false and AA_AB_Guard is true.
 *
 * This will execute:
 *
 * AAA_Exit
 * AA_Exit
 * AA_AB_Effect
 * AB_Entry
 * InitialAB_Effect
 * ABA_Entry.
 */
void test_HSM_Process_Event_TRIGGER_1_AAA_AAB_G_false_AA_AB_G_true_In_AAA_goes_to_ABA(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Composite_State_HSM.Current_State = &CSH_State_AAA;

    /* Setup expected call chain */
    CSH_AAA_AAB_Guard_ExpectAndReturn(CSH_TRIGGER_1, 0, false);
    CSH_AA_AB_Guard_ExpectAndReturn(CSH_TRIGGER_1, 0, true);

    CSH_AAA_Exit_Expect();
    CSH_AA_Exit_Expect();
    CSH_AA_AB_Effect_Expect();
    CSH_AB_Entry_Expect();
    CSH_InitialAB_Effect_Expect();
    CSH_ABA_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Composite_State_HSM, CSH_TRIGGER_1, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_ABA, Composite_State_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will take the transition from AAA
 * to ABA when it receives TRIGGER_1, AAA_AAB_Guard is false, AA_AB_Guard is false and A_B_Guard
 * is true.
 *
 * This will execute:
 *
 * AAA_Exit
 * AA_Exit
 * A_Exit
 * A_B_Effect
 * B_Entry
 * InitialB_Effect
 * BA_Entry
 * InitialBA_Effect
 * BAA_Entry
 */
void test_HSM_Process_Event_TRIGGER_1_AAA_AAB_G_false_AA_AB_G_false_A_B_G_true_In_AAA_goes_to_BAA(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Composite_State_HSM.Current_State = &CSH_State_AAA;

    /* Setup expected call chain */
    CSH_AAA_AAB_Guard_ExpectAndReturn(CSH_TRIGGER_1, 0, false);
    CSH_AA_AB_Guard_ExpectAndReturn(CSH_TRIGGER_1, 0, false);
    CSH_A_B_Guard_ExpectAndReturn(CSH_TRIGGER_1, 0, true);

    CSH_AAA_Exit_Expect();
    CSH_AA_Exit_Expect();
    CSH_A_Exit_Expect();
    CSH_A_B_Effect_Expect();
    CSH_B_Entry_Expect();
    CSH_InitialB_Effect_Expect();
    CSH_BA_Entry_Expect();
    CSH_InitialBA_Effect_Expect();
    CSH_BAA_Entry_Expect();

    /* Call function under test */
    HSM_Process_Event(&Composite_State_HSM, CSH_TRIGGER_1, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&CSH_State_BAA, Composite_State_HSM.Current_State);
}
