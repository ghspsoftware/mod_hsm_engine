#ifndef BRANCHING_COMPOSITE_STATE_HSM_H
#define BRANCHING_COMPOSITE_STATE_HSM_H
/**
 *  @file branching_transition_composite_state_hsm.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup branching_composite_state_hsm_imp
 *  @{
 *
 *      API that exposes the prototypes of all Guard and Behavior functions
 *      of the Branching Composite State HSM and the data structures used
 *      for mocking and testing.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    BTCSH_A_TRIGGER,
} Branching_Composite_HSM_Trigger_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
HSM_State_T const BTCSH_State_A;
HSM_State_T const BTCSH_State_AA;
HSM_State_T const BTCSH_State_AB;
HSM_State_T const BTCSH_State_AC;
HSM_State_T const BTCSH_State_AJ1;
HSM_State_T const BTCSH_State_AJ2;
HSM_State_T const BTCSH_State_B;
HSM_State_T const BTCSH_State_BA;
HSM_State_T const BTCSH_State_BB;
HSM_State_T const BTCSH_State_BC;
HSM_State_T const BTCSH_State_BJ1;
HSM_State_T const BTCSH_State_BJ2;
HSM_State_T const BTCSH_State_C;
HSM_State_T const BTCSH_State_CA;
HSM_State_T const BTCSH_State_CB;
HSM_State_T const BTCSH_State_CC;
HSM_State_T const BTCSH_State_CJ1;
HSM_State_T const BTCSH_State_CJ2;
HSM_State_T const BTCSH_State_Initial;
HSM_State_T const BTCSH_State_InitialA;
HSM_State_T const BTCSH_State_InitialB;
HSM_State_T const BTCSH_State_InitialC;
HSM_State_T const BTCSH_State_J1;
HSM_State_T const BTCSH_State_J2;

HSM_Transition_T const BTCSH_State_A_Outbound_Transitions[1];
HSM_Transition_T const BTCSH_State_AA_Outbound_Transitions[1];
HSM_Transition_T const BTCSH_State_AJ1_Outbound_Transitions[3];
HSM_Transition_T const BTCSH_State_AJ2_Outbound_Transitions[2];
HSM_Transition_T const BTCSH_State_BJ1_Outbound_Transitions[3];
HSM_Transition_T const BTCSH_State_CJ1_Outbound_Transitions[3];
HSM_Transition_T const BTCSH_State_J1_Outbound_Transitions[3];
HSM_Transition_T const BTCSH_State_J2_Outbound_Transitions[3];
HSM_Transition_T const BTCSH_State_Initial_Outbound_Transitions[1];
HSM_Transition_T const BTCSH_State_InitialA_Outbound_Transitions[1];
HSM_Transition_T const BTCSH_State_InitialB_Outbound_Transitions[1];
HSM_Transition_T const BTCSH_State_InitialC_Outbound_Transitions[1];

HSM_T Branching_Composite_HSM;
/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/*-----------------------*\
 * State Entry functions *
\*-----------------------*/
void BTCSH_A_Entry(void);
void BTCSH_AA_Entry(void);
void BTCSH_AB_Entry(void);
void BTCSH_AC_Entry(void);
void BTCSH_B_Entry(void);
void BTCSH_BA_Entry(void);
void BTCSH_BB_Entry(void);
void BTCSH_BC_Entry(void);
void BTCSH_C_Entry(void);
void BTCSH_CA_Entry(void);
void BTCSH_CB_Entry(void);
void BTCSH_CC_Entry(void);

/*-----------------------*\
 * State Exit functions *
\*-----------------------*/
void BTCSH_A_Exit(void);
void BTCSH_AA_Exit(void);

/*----------------------------*\
 * Transition Guard functions *
\*----------------------------*/
bool_t BTCSH_A_J2_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_AA_AJ2_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_AJ1_AA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_AJ1_AB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_AJ1_AC_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_AJ2_AB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_AJ2_AC_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_BJ1_BA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_BJ1_BB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_BJ1_BC_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_CJ1_CA_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_CJ1_CB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_CJ1_CC_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_J1_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_J1_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_J1_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_J2_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_J2_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t BTCSH_J2_C_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/*-----------------------------*\
 * Transition Effect functions *
\*-----------------------------*/
void BTCSH_A_J2_Effect(void);
void BTCSH_AA_AJ2_Effect(void);
void BTCSH_AJ1_AA_Effect(void);
void BTCSH_AJ1_AB_Effect(void);
void BTCSH_AJ1_AC_Effect(void);
void BTCSH_AJ2_AB_Effect(void);
void BTCSH_AJ2_AC_Effect(void);
void BTCSH_BJ1_BA_Effect(void);
void BTCSH_BJ1_BB_Effect(void);
void BTCSH_BJ1_BC_Effect(void);
void BTCSH_CJ1_CA_Effect(void);
void BTCSH_CJ1_CB_Effect(void);
void BTCSH_CJ1_CC_Effect(void);
void BTCSH_Initial_Effect(void);
void BTCSH_InitialA_Effect(void);
void BTCSH_InitialB_Effect(void);
void BTCSH_InitialC_Effect(void);
void BTCSH_J1_A_Effect(void);
void BTCSH_J1_B_Effect(void);
void BTCSH_J1_C_Effect(void);
void BTCSH_J2_A_Effect(void);
void BTCSH_J2_B_Effect(void);
void BTCSH_J2_C_Effect(void);

/** @} doxygen end group */
#endif /* BRANCHING_COMPOSITE_STATE_HSM_H */
