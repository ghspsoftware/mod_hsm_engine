/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "branching_transition_composite_state_hsm.h"
#include "hsm_engine.h"

/* MOCKS */
#include "mock_branching_transition_composite_state_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_basic_test_hsm.h"
#include "mock_branching_transition_hsm.h"
#include "mock_composite_state_hsm.h"
#include "mock_compound_transition_hsm.h"
#include "mock_else_guards_hsm.h"
#include "mock_internal_transitions_hsm.h"
#include "mock_multi_transition_hsm.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/*------------------------------------------------------------------*\
 *                    Initial Transition test cases                 *
\*------------------------------------------------------------------*/
/**
 * @test Verifies that the Statemachine engine will find a valid path to stable state AA
 * when the J1_A_Guard and AJ1_AA_Guard both are true. All other branching transition guards
 * are false.
 */
void test_HSM_Start_J1_A_G_AJ1_AA_G_true_initial_transition_Enters_AA(void)
{
    bool_t result = false;

    /* Ensure known test state */
    Branching_Composite_HSM.Current_State = NULL;

    /* Setup expected call chain */
    BTCSH_J1_A_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);
    BTCSH_AJ1_AA_Guard_ExpectAndReturn(HSM_NO_TRIGGER, 0, true);

    /* Order of guards is irrelevant. It does not matter if they are called or not. Ignore all other possible guard functions */
    BTCSH_J1_B_Guard_IgnoreAndReturn(false);
    BTCSH_J1_C_Guard_IgnoreAndReturn(false);

    BTCSH_Initial_Effect_Expect();
    BTCSH_J1_A_Effect_Expect();
    BTCSH_InitialA_Effect_Expect();
    BTCSH_A_Entry_Expect();
    BTCSH_AJ1_AA_Effect_Expect();
    BTCSH_AA_Entry_Expect();

    /* Call function under test */
    result = HSM_Start(&Branching_Composite_HSM);
    
    /* Verify test results */
    TEST_ASSERT_TRUE(result);
    TEST_ASSERT_EQUAL_PTR(&BTCSH_State_AA, Branching_Composite_HSM.Current_State);
}
