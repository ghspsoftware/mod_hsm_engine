#ifndef INTERNAL_TRANSITIONS_HSM_H
#define INTERNAL_TRANSITIONS_HSM_H
/**
 *  @file internal_transitions_hsm.h
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 *  @addtogroup internal_transitions_hsm_imp
 *  @{
 *
 *      API that exposes the prototypes of all Guard and Behavior functions
 *      of the Internal Transition HSM and data structures used for mocking
 *      and testing.
 */

/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "global.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Exported Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Exported Type Declarations
 *===========================================================================*/
typedef enum
{
    INTERNAL_A_TRIGGER,
    MULTILEVEL_TRIGGER
} Internal_Transition_HSM_Trigger_T;

/*===========================================================================*
 * Exported Const Object Declarations
 *===========================================================================*/
HSM_State_T const ITH_State_A;
HSM_State_T const ITH_State_AA;
HSM_State_T const ITH_State_AB;
HSM_State_T const ITH_State_B;
HSM_State_T const ITH_State_Initial;
HSM_State_T const ITH_State_InitialA;

HSM_Transition_T const ITH_State_A_Internal_Transitions[2];
HSM_Transition_T const ITH_State_A_Outbound_Transitions[1];
HSM_Transition_T const ITH_State_AA_Internal_Transitions[1];
HSM_Transition_T const ITH_State_AA_Outbound_Transitions[1];
HSM_Transition_T const ITH_State_Initial_Outbound_Transitions[1];
HSM_Transition_T const ITH_State_InitialA_Outbound_Transitions[1];

HSM_T Internal_Transitions_HSM;

/*===========================================================================*
 * Exported Function Prototypes
 *===========================================================================*/
/*-----------------------*\
 * State Entry functions *
\*-----------------------*/

/*-----------------------*\
 * State Exit functions *
\*-----------------------*/

/*----------------------------*\
 * Transition Guard functions *
\*----------------------------*/
bool_t ITH_A_B_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t ITH_AA_AB_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t ITH_AA_Internal_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t ITH_Internal_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);
bool_t ITH_Multilevel_A_Guard(HSM_Trigger_T Trigger, uint32_t Trigger_Data);

/*-----------------------------*\
 * Transition Effect functions *
\*-----------------------------*/
void ITH_A_B_Effect(void);
void ITH_AA_AB_Effect(void);
void ITH_AA_Internal_Effect(void);
void ITH_Internal_A_Effect(void);
void ITH_Multilevel_A_Effect(void);

/** @} doxygen end group */
#endif /* INTERNAL_TRANSITIONS_HSM_H */
