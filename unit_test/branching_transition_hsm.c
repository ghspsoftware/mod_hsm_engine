/**
 *  @file basic_test_hsm.c
 *
 *  @copyright 2018 GHSP, Inc., All Rights Reserved.@n
 *  GHSP Confidential @n
 *  @b NOTICE: @n
 *  All information contained herein is, and remains the property of GHSP, Inc.
 *  The intellectual and technical concepts contained herein are proprietary to
 *  GHSP, Inc. and may be covered by U.S. and Foreign Patents, patents in
 *  process, and are protected by trade secret or copyright law. Dissemination
 *  of this information or reproduction of this material is strictly forbidden
 *  unless prior written permission is obtained from GHSP, Inc.
 *
 */
/*===========================================================================*
 * Header Files
 *===========================================================================*/
#include "branching_transition_hsm.h"
#include "hsm_engine.h"

/*===========================================================================*
 * Local Preprocessor \#define Constants
 *===========================================================================*/

/*===========================================================================*
 * Local Preprocessor \#define MACROS
 *===========================================================================*/

/*===========================================================================*
 * Exported Preprocessor \#define function-like Macros
 *===========================================================================*/

/*===========================================================================*
 * Local Type Declarations
 *===========================================================================*/

/*===========================================================================*
 * Exported Const Object Definitions
 *===========================================================================*/

/*===========================================================================*
 * Local Function Prototypes
 *===========================================================================*/

/*===========================================================================*
 * Local Object Definitions
 *===========================================================================*/
/*-----------------------------------------------------*\
 * Begin: Branching Transition Statemachine definition *
\*-----------------------------------------------------*/

HSM_Transition_T const BRTH_State_A_Outbound_Transitions[2] =
{
    {
        .Trigger = BRTH_A_J5_TRIGGER,
        .Guard = BRTH_A_J5_Guard,
        .Effect = BRTH_A_J5_Effect,
        .Source = &BRTH_State_A,
        .Target = &BRTH_State_J5,
    },
    {
        .Trigger = BRTH_A_B_TRIGGER,
        .Guard = BRTH_A_B_Guard,
        .Effect = BRTH_A_B_Effect,
        .Source = &BRTH_State_A,
        .Target = &BRTH_State_B,
    },
};

HSM_Transition_T const BRTH_State_D_Outbound_Transitions[1] =
{
    {
        .Trigger = BRTH_D_J5_TRIGGER,
        .Guard = BRTH_D_J5_Guard,
        .Effect = BRTH_D_J5_Effect,
        .Source = &BRTH_State_D,
        .Target = &BRTH_State_J5,
    },
};

HSM_Transition_T const BRTH_State_Initial_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = BRTH_Initial_Effect,
        .Source = &BRTH_State_Initial,
        .Target = &BRTH_State_J1,
    },
};

HSM_Transition_T const BRTH_State_J1_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J1_J2_Guard,
        .Effect = BRTH_J1_J2_Effect,
        .Source = &BRTH_State_J1,
        .Target = &BRTH_State_J2,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J1_J3_Guard,
        .Effect = BRTH_J1_J3_Effect,
        .Source = &BRTH_State_J1,
        .Target = &BRTH_State_J3,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J1_J4_Guard,
        .Effect = BRTH_J1_J4_Effect,
        .Source = &BRTH_State_J1,
        .Target = &BRTH_State_J4,
    },
};

HSM_Transition_T const BRTH_State_J2_Outbound_Transitions[3] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J2_A_Guard,
        .Effect = BRTH_J2_A_Effect,
        .Source = &BRTH_State_J2,
        .Target = &BRTH_State_A,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J2_J8_Guard,
        .Effect = NULL,
        .Source = &BRTH_State_J2,
        .Target = &BRTH_State_J8,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J2_C_Guard,
        .Effect = BRTH_J2_C_Effect,
        .Source = &BRTH_State_J2,
        .Target = &BRTH_State_C,
    },
};

HSM_Transition_T const BRTH_State_J3_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J3_B_Guard,
        .Effect = BRTH_J3_B_Effect,
        .Source = &BRTH_State_J3,
        .Target = &BRTH_State_B,
    },
};

HSM_Transition_T const BRTH_State_J4_Outbound_Transitions[2] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J4_B_Guard,
        .Effect = BRTH_J4_B_Effect,
        .Source = &BRTH_State_J4,
        .Target = &BRTH_State_B,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J4_J7_Guard,
        .Effect = NULL,
        .Source = &BRTH_State_J4,
        .Target = &BRTH_State_J7,
    },
};

HSM_Transition_T const BRTH_State_J5_Outbound_Transitions[4] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J5_A_Guard,
        .Effect = BRTH_J5_A_Effect,
        .Source = &BRTH_State_J5,
        .Target = &BRTH_State_A,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J5_J6_Guard,
        .Effect = BRTH_J5_J6_Effect,
        .Source = &BRTH_State_J5,
        .Target = &BRTH_State_J6,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J5_J9_Guard,
        .Effect = NULL,
        .Source = &BRTH_State_J5,
        .Target = &BRTH_State_J9,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J5_J10_Guard,
        .Effect = BRTH_J5_J10_Effect,
        .Source = &BRTH_State_J5,
        .Target = &BRTH_State_J10,
    },
};

HSM_Transition_T const BRTH_State_J6_Outbound_Transitions[4] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J6_A_Guard,
        .Effect = BRTH_J6_A_Effect,
        .Source = &BRTH_State_J6,
        .Target = &BRTH_State_A,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J6_B_Guard,
        .Effect = BRTH_J6_B_Effect,
        .Source = &BRTH_State_J6,
        .Target = &BRTH_State_B,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J6_C_Guard,
        .Effect = BRTH_J6_C_Effect,
        .Source = &BRTH_State_J6,
        .Target = &BRTH_State_C,
    },
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J6_D_Guard,
        .Effect = NULL,
        .Source = &BRTH_State_J6,
        .Target = &BRTH_State_D,
    },
};

HSM_Transition_T const BRTH_State_J7_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = BRTH_J7_C_Effect,
        .Source = &BRTH_State_J7,
        .Target = &BRTH_State_C,
    },
};

HSM_Transition_T const BRTH_State_J8_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = NULL,
        .Source = &BRTH_State_J8,
        .Target = &BRTH_State_B,
    },
};

HSM_Transition_T const BRTH_State_J9_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = NULL,
        .Effect = BRTH_J9_C_Effect,
        .Source = &BRTH_State_J9,
        .Target = &BRTH_State_C,
    },
};

HSM_Transition_T const BRTH_State_J10_Outbound_Transitions[1] =
{
    {
        .Trigger = HSM_NO_TRIGGER,
        .Guard = BRTH_J10_C_Guard,
        .Effect = BRTH_J10_C_Effect,
        .Source = &BRTH_State_J10,
        .Target = &BRTH_State_C,
    },
};

HSM_State_T const BRTH_State_A =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_A_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_A_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BRTH_A_Entry,
    .Exit_Behavior = BRTH_A_Exit,
    .Name = "A",
};

HSM_State_T const BRTH_State_B =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BRTH_B_Entry,
    .Exit_Behavior = BRTH_B_Exit,
    .Name = "B",
};

HSM_State_T const BRTH_State_C =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 0,
    .Outbound_Transitions = NULL,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BRTH_C_Entry,
    .Exit_Behavior = BRTH_C_Exit,
    .Name = "C",
};

HSM_State_T const BRTH_State_D =
{
    .Kind = HSM_SIMPLE,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_D_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_D_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = BRTH_D_Entry,
    .Exit_Behavior = BRTH_D_Exit,
    .Name = "D",
};

HSM_State_T const BRTH_State_Initial =
{
    .Kind = HSM_INITIAL,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = 1,
    .Outbound_Transitions = BRTH_State_Initial_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "Initial",
};

HSM_State_T const BRTH_State_J1 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J1_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J1_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J1",
};

HSM_State_T const BRTH_State_J2 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J2_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J2_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J2",
};

HSM_State_T const BRTH_State_J3 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J3_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J3_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J3",
};

HSM_State_T const BRTH_State_J4 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J4_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J4_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J4",
};

HSM_State_T const BRTH_State_J5 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J5_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J5_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J5",
};

HSM_State_T const BRTH_State_J6 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J6_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J6_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J6",
};

HSM_State_T const BRTH_State_J7 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J7_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J7_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J7",
};

HSM_State_T const BRTH_State_J8 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J8_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J8_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J8",
};

HSM_State_T const BRTH_State_J9 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J9_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J9_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J9",
};

HSM_State_T const BRTH_State_J10 =
{
    .Kind = HSM_JUNCTION,
    .Initial_State = NULL,
    .Parent_State = NULL,
    .Num_Outbound_Transitions = Num_Elems(BRTH_State_J10_Outbound_Transitions),
    .Outbound_Transitions = BRTH_State_J10_Outbound_Transitions,
    .Num_Internal_Transitions = 0,
    .Internal_Transitions = NULL,
    .Entry_Behavior = NULL,
    .Exit_Behavior = NULL,
    .Name = "J10",
};

HSM_T Branching_Transition_HSM =
{
    .Current_State = NULL,
    .Initial_State = &BRTH_State_Initial,
};

/*---------------------------------------------------*\
 * End: Branching Transition Statemachine definition *
\*---------------------------------------------------*/
/*===========================================================================*
 * Function Definitions
 *===========================================================================*/
