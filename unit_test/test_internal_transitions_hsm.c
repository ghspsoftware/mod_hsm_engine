/**
 *  @file test_hsm_engine.c
 *
 */
#include "unity.h"
#include "hsm_engine.h"
#include "internal_transitions_hsm.h"

/* MOCKS */
#include "mock_internal_transitions_hsm.h"

/* Add mocks for other statecharts. This is a limitation of GHSP Make */
#include "mock_basic_test_hsm.h"
#include "mock_branching_transition_hsm.h"
#include "mock_branching_transition_composite_state_hsm.h"
#include "mock_composite_state_hsm.h"
#include "mock_compound_transition_hsm.h"
#include "mock_else_guards_hsm.h"
#include "mock_multi_transition_hsm.h"

/* STATIC's */

void setUp(void)
{

}

void tearDown(void)
{

}

/**
 * @test Verifies that the Statemachine engine will enable the internal
 * transition of State A when in State AA and it received INTERNAL_A_TRIGGER
 * with Internal_A_Guard true
 */
void test_HSM_Process_Event_INTERNAL_A_TRIGGER_In_AA_Triggers_Internal_Transition_for_A(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AA;

    /* Setup expected call chain */
    ITH_Internal_A_Guard_ExpectAndReturn(INTERNAL_A_TRIGGER, 0, true);
    ITH_Internal_A_Effect_Expect();

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, INTERNAL_A_TRIGGER, 0);
    
    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_AA, Internal_Transitions_HSM.Current_State);
}


/**
 * @test Verifies that the Statemachine engine will enable the internal
 * transition of State A when in State AB and it received INTERNAL_A_TRIGGER
 * with Internal_A_Guard true
 */
void test_HSM_Process_Event_INTERNAL_A_TRIGGER_In_AB_Triggers_Internal_Transition_for_A(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AB;

    /* Setup expected call chain */
    ITH_Internal_A_Guard_ExpectAndReturn(INTERNAL_A_TRIGGER, 0, true);
    ITH_Internal_A_Effect_Expect();

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, INTERNAL_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_AB, Internal_Transitions_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will not enable any transition
 * when it receives the INTERNAL_A_TRIGGER event while in B.
 */
void test_HSM_Process_Event_INTERNAL_A_TRIGGER_In_B_Does_Nothing(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_B;

    /* Setup expected call chain */

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, INTERNAL_A_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_B, Internal_Transitions_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will enable the AA Internal transition when
 * it receives the MULTILEVEL_TRIGGER event while in AA and the AA Internal Guard is true.
 */
void test_HSM_Process_Event_MULTILEVEL_TRIGGER_In_AA_AA_Internal_Guard_true_Enables_AA_Internal_Transition(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AA;

    /* Setup expected call chain */
    ITH_AA_Internal_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, true);
    ITH_AA_Internal_Effect_Expect();

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, MULTILEVEL_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_AA, Internal_Transitions_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will enable the A Internal transition when
 * it receives the MULTILEVEL_TRIGGER event while in AA and the following Guard conditions:
 *
 * AA_Internal_Guard is false
 * AA_AB_Guard is false
 * Multilevel_A_Guard is true
 *
 */
void test_HSM_Process_Event_MULTILEVEL_TRIGGER_In_AA_A_Internal_Guard_true_Enables_A_Internal_Transition(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AA;

    /* Setup expected call chain */
    ITH_AA_Internal_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_AA_AB_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_Multilevel_A_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, true);
    ITH_Multilevel_A_Effect_Expect();

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, MULTILEVEL_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_AA, Internal_Transitions_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will enable the A Internal transition when
 * it receives the MULTILEVEL_TRIGGER event while in AB and the following Guard conditions:
 *
 * Internal_A_Guard is true
 *
 */
void test_HSM_Process_Event_MULTILEVEL_TRIGGER_In_AB_A_Internal_Guard_true_Enables_A_Internal_Transition(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AB;

    /* Setup expected call chain */
    ITH_Multilevel_A_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, true);
    ITH_Multilevel_A_Effect_Expect();

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, MULTILEVEL_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_AB, Internal_Transitions_HSM.Current_State);
}


/**
 * @test Verifies that the Statemachine engine will enable the AA to AB external transition when
 * it receives the MULTILEVEL_TRIGGER event while in AA and the following Guard conditions:
 *
 * AA_Internal_Guard is false
 * AA_AB_Guard is true
 *
 */
void test_HSM_Process_Event_MULTILEVEL_TRIGGER_In_AA_AA_AB_Guard_true_Enables_AA_AB_Transition(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AA;

    /* Setup expected call chain */
    ITH_AA_Internal_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_AA_AB_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, true);
    ITH_AA_AB_Effect_Expect();

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, MULTILEVEL_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_AB, Internal_Transitions_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will enable the A to B external transition when
 * it receives the MULTILEVEL_TRIGGER event while in AA and the following Guard conditions:
 *
 * AA_Internal_Guard is false
 * AA_AB_Guard is false
 * Multilevel_A_Guard is false
 * A_B_Guard is true
 */
void test_HSM_Process_Event_MULTILEVEL_TRIGGER_In_AA_A_B_Guard_true_Enables_A_B_Transition(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AA;

    /* Setup expected call chain */
    ITH_AA_Internal_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_AA_AB_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_Multilevel_A_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_A_B_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, true);
    ITH_A_B_Effect_Expect();

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, MULTILEVEL_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_B, Internal_Transitions_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will enable the A to B external transition when
 * it receives the MULTILEVEL_TRIGGER event while in AB and the following Guard conditions:
 *
 * Multilevel_A_Guard is false
 * A_B_Guard is true
 */
void test_HSM_Process_Event_MULTILEVEL_TRIGGER_In_AB_A_B_Guard_true_Enables_A_B_Transition(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AB;

    /* Setup expected call chain */
    ITH_Multilevel_A_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_A_B_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, true);
    ITH_A_B_Effect_Expect();

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, MULTILEVEL_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_B, Internal_Transitions_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will not enable any transition when it
 * receives the MULTILEVEL_TRIGGER event while in AA with the following Guard conditions:
 *
 * AA_Internal_Guard is false
 * AA_AB_Guard is false
 * Multilevel_A_Guard is false
 * A_B_Guard is false
 */
void test_HSM_Process_Event_MULTILEVEL_TRIGGER_In_AA_All_Guards_false_Does_Nothing(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AA;

    /* Setup expected call chain */
    ITH_AA_Internal_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_AA_AB_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_Multilevel_A_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_A_B_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, MULTILEVEL_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_AA, Internal_Transitions_HSM.Current_State);
}

/**
 * @test Verifies that the Statemachine engine will not enable any transition when it
 * receives the MULTILEVEL_TRIGGER event while in AB with the following Guard conditions:
 *
 * Multilevel_A_Guard is false
 * A_B_Guard is false
 */
void test_HSM_Process_Event_MULTILEVEL_TRIGGER_In_AB_All_Guards_false_Does_Nothing(void)
{
    /* Ensure known test state */
    Internal_Transitions_HSM.Current_State = &ITH_State_AB;

    /* Setup expected call chain */
    ITH_Multilevel_A_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);
    ITH_A_B_Guard_ExpectAndReturn(MULTILEVEL_TRIGGER, 0, false);

    /* Call function under test */
    HSM_Process_Event(&Internal_Transitions_HSM, MULTILEVEL_TRIGGER, 0);

    /* Verify test results */
    TEST_ASSERT_EQUAL_PTR(&ITH_State_AB, Internal_Transitions_HSM.Current_State);
}
